## Interface: 11200
## Title: Carbonite 5.038
## Version: 5.038
## Notes: Backport for 1.12.1 by Ritual
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: NxData, NxCombatOpts, NxMapOpts
## SavedVariablesPerCharacter: NxCData

Localization.lua
NxLocalization.lua
Carbonite.lua
Data\data.xml
NxUI.lua
NxHelp.lua
NxOptions.lua
NxCom.lua
NxFav.lua
NxHUD.lua
NxInfo.lua
NxMapData.lua
NxMap.lua
NxMapGuide.lua
NxQuest.lua
NxSocial.lua
NxTravel.lua
NxWarehouse.lua
NxSec.lua
NxTimer.lua
MapWorldHotspots.lua
ZoneConnections.lua
Zones.lua
Carbonite.xml
