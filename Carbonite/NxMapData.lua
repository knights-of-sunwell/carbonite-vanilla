﻿---------------------------------------------------------------------------------------
-- NxMapData - Map code
-- Copyright 2007-2012 Carbon Based Creations, LLC
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
-- Carbonite - Addon for World of Warcraft(tm)
-- Copyright 2007-2012 Carbon Based Creations, LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------------------------

--[[

4 = "Durotar",10.57499926,392.499976,-361.66665
9 = "Mulgore",10.27499976,-409.583326,54.5833312
11 = "Barrens",20.266666,-524.5833,-322.499976
13 = "Kalimdor",73.59962,-3413.32,-2559.98
14 = "Azeroth",70.3998,-3200.0,-1493.32
15 = "Alterac",5.59999986,-156.66666,-300.0
16 = "Arathi",7.1999998,173.33332,26.6666656
17 = "Badlands",4.975,415.8333,1177.9166
19 = "BlastedLands",6.69999974,248.333326,2113.3332
20 = "Tirisfal",9.03749976,-606.66665,-767.499952
21 = "Silverpine",8.39999952,-689.999952,-333.333326
22 = "WesternPlaguelands",8.599999812,-83.3333312,-673.3333
23 = "EasternPlaguelands",7.741667,437.0833,-759.999952
24 = "Hilsbrad",6.39999976,-213.333326,-80.0
26 = "Hinterlands",7.7,315.0,-293.333326
27 = "DunMorogh",9.8499995,-360.41665,775.41665
28 = "SearingGorge",4.462499688,64.5833312,1220.0
29 = "BurningSteppes",5.858333188,53.3333312,1406.249902
30 = "Elwynn",6.94166652,-307.083326,1587.9166
32 = "DeadwindPass",4.9999999,166.66666,1973.3332
34 = "Duskwood",5.39999986,-166.66666,1943.3332
35 = "LochModan",5.51666624,398.749976,897.5
36 = "Redridge",4.3416665,314.16665,1715.0
37 = "Stranglethorn",12.7624995,-444.16665,2233.75
38 = "SwampOfSorrows",4.5875,444.5833,1924.1666
39 = "Westfall",6.9999996,-603.3333,1880.0
40 = "Wetlands",8.2708334,77.91666,429.5833
41 = "Teldrassil",10.183333,-762.91665,-2366.25
42 = "Darkshore",13.0999995,-588.3333,-1666.6666
43 = "Ashenvale",11.53333276,-339.999976,-934.5833
61 = "ThousandNeedles",8.7999994,86.66666,793.3333
81 = "StonetalonMountains",9.76666626,-649.16665,-583.3333
101 = "Desolace",8.991666,-846.6666,-90.41666
121 = "Feralas",13.8999995,-1088.3333,473.3333
141 = "Dustwallow",10.500000122,194.9999878,406.66665
161 = "Tanaris",13.79999905,43.749997,1175.0
181 = "Aszhara",10.1416655,655.41665,-1068.3333
182 = "Felwood",11.49999926,-328.333326,-1426.6666
201 = "UngoroCrater",7.3999996,-106.66666,1193.3333
241 = "Moonglade",4.6166665,276.25,-1698.3332
261 = "Silithus",6.966668,-507.5,1191.6668
281 = "Winterspring",14.199999688,63.3333312,-1706.6666
301 = "Stormwind",3.474999926,-344.583326,1599.1666
321 = "Ogrimmar",2.80520914,736.1202,-454.77544
341 = "Ironforge",1.58125006,142.71828,913.8482
362 = "ThunderBluff",2.0874998,-103.33332,169.9999878
381 = "Darnassis",2.1166665,-587.672558,-2047.66328
382 = "Undercity",1.91875,-174.63852,-375.589062
401 = "AlteracValley",8.47499976,-356.249976,-217.083326
443 = "WarsongGulch",2.29166666,-408.333326,-325.41665
461 = "ArathiBasin",3.512499844,-371.66665,-301.66665
462 = "EversongWoods",9.85,897.5,-2208.3332
463 = "Ghostlands",6.6,1056.6666,-1653.3332
480 = "SilvermoonCity",2.422917,1280.15,-2030.7418
500 = "Collin",75.3029062,-3410.36718,-1838.85
501 = "AzsharaCrater",8.472,-487.2,-353.0
502 = "Hyjal",6.41326164,216.037036,-1225.0622
503 = "ScarletEnclave",6.344,806.6,-619.4
504 = "Sunnyglade",1.977252,-143.8938,-400.1538
505 = "Lapidis",4.330132,-905.624,2186.33848
506 = "Gillijim",4.929888,-821.3808,2532.7127
507 = "AlteracValleyClassic",8.47499976,-356.249976,-217.083326
508 = "TelAbim",6.308,1043.2,1438.4
509 = "AmaniAlor",3.03,-626.0,-686.2
511 = "AlahThalas",2.02189264,437.758936,-913.6
513 = "WinterVeilVale",2.864,-383.2,466.4
515 = "Ragefire",1.477728,-90.5742,-7.92464
517 = "ZulFarrak",2.766666448,-324.999976,-410.41665
519 = "TheTempleOfAtalHakkar",1.390058,-88.5516,51.0332
521 = "BlackFathomDeeps",2.44374,-97.082,21.582
523 = "TheStockade",0.756306,-37.9644,-44.1266
525 = "Gnomeregan",1.539336,-98.3792,36.1776
527 = "Uldaman",1.787336,-129.0238,-40.8612
528 = "MoltenCore",2.5296,26.152,-260.612
529 = "ZulGurub",4.2416665,122.5,2245.0
531 = "DireMaul",2.55,-177.5,-210.0
533 = "BlackrockDepths",2.814122,-104.4674,-237.336084
535 = "RuinsofAhnQiraj",5.0249998,-607.0833,1646.6666
537 = "OnyxiasLair",0.966236,-22.2766,-19.64916
539 = "BlackrockSpire",1.773678,-2.1174,-60.8796
541 = "WailingCaverns",1.87295,-112.1058,-42.834
543 = "Maraudon",4.22418,-97.082,-239.952
545 = "BlackwingLair",0.998856,168.9244,1478.824
547 = "TheDeadmines",1.31318,64.588,-7.386
549 = "RazorfenDowns",1.418098,-255.988,-536.51
551 = "RazorfenKraul",1.4729,-411.784,-469.928
553 = "ScarletMonastery",1.239968,-313.594,-406.03628
554 = "ScarletMonastery2f",0.640382,32.4546,-61.4732
555 = "ScarletMonastery3f",1.2253932,-10.05452,-401.82
556 = "ScarletMonastery4f",1.4066,-348.798,-256.258082
558 = "Scholomance",0.6400978,-50.282,-63.5396
560 = "ShadowfangKeep",0.7048602,-471.24,16.98592
562 = "Stratholme",2.370688,527.9668,-832.7792
564 = "AhnQiraj",1.95512,-503.126,1610.359278
565 = "AhnQiraj2f",5.555088,-583.124,1531.8574
567 = "Karazhan",1.19608104,322.836036,2156.12266
569 = "DeeprunTram",0.624,-524.7,-19.1
570 = "DeeprunTram2f",0.618,-37.5,-19.1
572 = "BlackMorass",2.543982,-1467.6932,249.510938
573 = "BlackMorass2f",2.17171732,-1530.369532,300.094654
575 = "GilneasCity",2.50038,-658.065,152.1432
577 = "Naxxramas",3.98338,475.055,-731.34
578 = "Naxxramas2f",1.3042,973.872,-763.312
579 = "DeadminesEntrance",0.89978,-358.636,2210.988
580 = "WailingCavernsEntrance",1.145554,380.174,97.7274
581 = "MaraudonEntrance",1.648,-641.6,219.2
582 = "GnomereganEntrance",1.14238,-205.528,961.264
583 = "BlackrockMountain",1.42312,152.188,1465.564
584 = "ScarletMonasteryEntrance",0.40732,131.992,-589.474
585 = "UldamanEntrance",1.12662,549.43,1190.682
586 = "AhnQirajEntrance",8.27804,-786.534,1601.04
587 = "CavernsOfTime",2.69648468,739.169532,1604.618262
588 = "CrescentGrove",5.28643,-241.544,-176.5952
589 = "HateforgeQuarry",1.504238,604.8666,1577.6558
590 = "KarazhanCrypt",1.0935,260.5018,2183.23458
591 = "StormwindVault",0.708994,-62.8678,-17.8598
592 = "Gilneas",7.335276,-650.7708,71.9306
593 = "Moomoo",2.0153652,-3459.25976,-3431.7043
594 = "EmeraldSanctum",2.54620264,-787.820264,-703.9444
595 = "Icepoint",3.193875,1123.15625,-2879.4246
--]]

-- Local

local Map = Nx.Map

--------
-- Map tables

Map.MapInfo = {
	[0] = {	-- Dummy
		Name = "Instance",
        FileName = "World",
		X = 2200,
	    -- Y = 500,
        Y = 0,
	},
	{
		Name = "Kalimdor",
		FileName = "Kalimdor",
		X = 0,
		Y = 500,
		Min = 1001,
		Max = 1034,
	},
    {
		Name = "Eastern Kingdoms",
		FileName = "Azeroth",
        X = 3784,
		Y = -200,
		Min = 2001,
		Max = 2040,
	},
	[90] = {
		Name = "BG",
		X = 2000,
		Y = 500,
	},
}

Map.BXO=-503-15
Map.BYO=516+40
Map.DXO=-3500
Map.DYO=-2010+100 -- 100 сдвиг на карте сируса

Map.MapWorldInfo = {
[0]={10,0,0,0,0,Overlay="ashenvale",},
-- [0]={75.3029062,-3410.36718,-1838.85},
[1000]={73.3282,-3398.85,-2552.91,},
[1001]={Name="Ashenvale",11.53329,-339.9963,-934.5858,Overlay="ashenvale",Fish = 150,},
[1002]={Name="Azshara",10.1416,655.4186,-1068.333,Overlay="aszhara",Fish = 300,},
[1003]={Name="Azuremyst Isle",8.141665,Map.DXO+2100.001,Map.DYO+558.7514,Overlay="azuremystisle",MId=1003,Fish = 25,},
[1004]={Name="Bloodmyst Isle",6.525004,Map.DXO+2015,Map.DYO+151.667,Overlay="bloodmystisle",MId=1003,Fish = 75,},
[1005]={Name="Darkshore",13.10007,-588.3355,-1666.666,Overlay="darkshore",Fish = 75,},
[1006]={Name="Darnassus",2.116669,-587.6726,-2047.663,Overlay="darnassis",City = true,MMOutside = true,Fish = 75,},
[1007]={Name="Desolace",8.99169,-846.6668,-90.41775,Overlay="desolace",Fish = 225,},
[1008]={Name="Durotar",10.575,392.5,-361.6666,Overlay="durotar",Fish = 25,},
[1009]={Name="Dustwallow Marsh",10.500006,195.0004,406.6614,Overlay="dustwallow",Fish = 225,},
[1010]={Name="Felwood",11.50004,-328.3354,-1426.666,Overlay="felwood",Fish = 300,},
[1011]={Name="Feralas",13.89998,-1088.332,473.3343,Overlay="feralas",Fish = 300,},
[1012]={Name="Moonglade",4.616656,276.2508,-1698.333,Overlay="moonglade",Fish = 300,},
[1013]={Name="Mulgore",10.275012,-409.5834,54.58379,Overlay="mulgore",Fish = 25,},
[1014]={Name="Orgrimmar",2.805208,736.1202,-454.7754,Overlay="ogrimmar",City = true,Fish = 75,},
[1015]={Name="Silithus",6.966681,-507.5004,1191.667,Overlay="silithus",Fish = 425,},
[1016]={Name="Stonetalon Mountains",9.766648,-649.1656,-583.331,Overlay="stonetalonmountains",Fish = 150,},
[1017]={Name="Tanaris",13.79999,43.75034,1175,Overlay="tanaris",Fish = 300,},
[1018]={Name="Teldrassil",10.18333,-762.9161,-2366.25,Overlay="teldrassil",Fish = 25,},
[1019]={Name="The Barrens",20.26656,-524.5772,-322.4962,Overlay="barrens",Fish = 75,},
[1020]={Name="The Exodar",2.113537,Map.DXO+2213.274,Map.DYO+721.9364,Overlay="theexodar",City = true,MId=1003,},
[1021]={Name="Thousand Needles",8.800012,86.6665,793.3343,Overlay="thousandneedles",Fish = 225,},
[1022]={Name="Thunder Bluff",2.087504,-103.3333,170,Overlay="thunderbluff",City = true,MMOutside = true,Fish = 75,},
[1023]={Name="Un'Goro Crater",7.399998,-106.6661,1193.333,Overlay="ungorocrater",Fish = 300,},
[1024]={Name="Winterspring",14.20029,63.32512,-1706.666,Overlay="winterspring",Fish = 425,},
--new (turtle)
[1025]={Name="Amani'Alor",3.03,-626.0,-686.2,Overlay="amanialor",Fish = 425,Explored = true},
[1026]={Name="Caverns of Time",2.69648468,739.169532,1604.618262,Overlay="winterspring",Fish = 425,City = true},
[1027]={Name="Gates of Ahn'Qiraj",8.27804,-786.534,1601.04,Overlay="winterspring",Fish = 425,City = true},
[1028]={Name="Hyjal",6.41326164,216.037036,-1225.0622,Overlay="hyjal",Fish = 425,Explored = true},
[1029]={Name="Icepoint Rock",3.193875,1123.15625,-2879.4246,Overlay="icepoint",Fish = 425,Explored = true},
[1030]={Name="Maraudon",1.648,-641.6,219.2,Overlay="winterspring",Fish = 425,City = true},
[1031]={Name="Tel'Abim",6.308,1043.2,1438.4,Overlay="telabim",Fish = 425,ScaleAdjY=1.034,Explored = true},
[1032]={Name="Wailing Caverns",1.145554,380.174,97.7274,Overlay="winterspring",Fish = 425,City = true},
[1033]={Name="Blackstone Island",4.944, 1254.8, -159.8,Overlay="blackstoneisland",Fish = 425,},
[1034]={Name="GM Island",1.656,-3349.6,-3305,Overlay="gmisland",Fish = 425,Explored = true},
--
-- [2000]={81.53,-3645.96,-2249.31,},
[2000]={70.3998,-3200.,-1493.32,},
[2001]={Name="Alterac Mountains",5.599993,-156.6661,-299.9998,Overlay="alterac",Fish = 225,},
[2002]={Name="Arathi Highlands",7.199987,173.3343,26.66715,Overlay="arathi",Fish = 225,},
[2003]={Name="Badlands",4.974991,415.8339,1177.917,Overlay="badlands",},
[2004]={Name="Blasted Lands",6.699956,248.3361,2113.333,Overlay="blastedlands",},
[2005]={Name="Burning Steppes",5.85836,53.33179,1406.25,Overlay="burningsteppes",Fish = 425,},
[2006]={Name="Deadwind Pass",4.999989,166.6672,1973.333,Overlay="deadwindpass",Fish = 425,},
[2007]={Name="Dun Morogh",9.849867,-360.4126,775.4073,Overlay="dunmorogh",Fish = 25,},
[2008]={Name="Duskwood",5.399999,-166.6662,1943.333,Overlay="duskwood",Fish = 150,},
[2009]={Name="Eastern Plaguelands",7.741667,437.0833,-759.999952,Overlay="easternplaguelands",Fish = 425,},
[2010]={Name="Elwynn Forest",6.941641,-307.082,1587.917,Overlay="elwynn",Fish = 25,},
[2011]={Name="Eversong Woods",9.850039,Map.BXO+897.499,Map.BYO-2208.334,Overlay="eversongwoods",MId=2011,Fish = 25,},
[2012]={Name="Ghostlands",6.600027,Map.BXO+1056.666,Map.BYO-1653.333,Overlay="ghostlands",MId=2011,Fish = 75,},
[2013]={Name="Hillsbrad Foothills",6.399936,-213.3293,-79.99989,Overlay="hilsbrad",Fish = 150,},
[2014]={Name="Ironforge",1.581249,142.7185,913.8483,Overlay="ironforge",City = true,Fish = 75,},
[2015]={Name="Loch Modan",5.516659,398.7504,897.5004,Overlay="lochmodan",Fish = 75,},
[2016]={Name="Redridge Mountains",4.341669,314.1668,1715,Overlay="redridge",Fish = 150,},
[2017]={Name="Searing Gorge",4.462489,64.58443,1220,Overlay="searinggorge",},
[2018]={Name="Silvermoon City",2.42292,Map.BXO+1280.15,Map.BYO-2030.742,Overlay="silvermooncity",City = true,MId=2011,},
[2019]={Name="Silverpine Forest",8.399968,-689.998,-333.3325,Overlay="silverpine",Fish = 75,},
[2020]={Name="Stormwind City",3.4732,-344.46097,1599.206616,Overlay="stormwind",City = true,Fish = 75,},
[2021]={Name="Stranglethorn Vale",12.76268,-444.1722,2233.75,Overlay="stranglethorn",Fish = 225,},
[2022]={Name="Swamp of Sorrows",4.587497,444.5835,1924.166,Overlay="swampofsorrows",Fish = 225,},
[2023]={Name="The Hinterlands",7.699974,315.0007,-293.3329,Overlay="hinterlands",Fish = 300,},
[2024]={Name="Tirisfal Glades",9.037504,-606.6664,-767.4979,Overlay="tirisfal",Fish = 25,},
[2025]={Name="Undercity",1.9187478,-174.6383,-375.589,Overlay="undercity",City = true,Fish = 75,},
[2026]={Name="Western Plaguelands",8.599958,-83.33159,-673.3347,Overlay="westernplaguelands",Fish = 300,},
[2027]={Name="Westfall",7.000001,-603.3333,1880.002,Overlay="westfall",Fish = 75,},
[2028]={Name="Wetlands",8.27078,77.92012,429.5833,Overlay="wetlands",Fish = 150,},
[2029]={Name="Isle of Quel'Danas",6.655,Map.BXO+1060.404,Map.BYO-2713.859,Overlay="sunwell",MId=2011,Fish = 450,},
[2030]={Name="Scarlet Enclave",6.344,806.6,-619.4,Overlay="scarletenclave",Fish = 150,},
--new (turtle)
[2031]={Name="Alah'Thalas",2.936,433.8,-981.4,Overlay="alahthalas",Fish = 150,City = true},
[2032]={Name="Blackrock Mountain",1.42312,152.188,1465.564,Overlay="blackrock",Fish = 150,City = true},
[2033]={Name="Gillijim's Isle",4.929888,-821.3808,2532.7127,Overlay="gillijim",Fish = 150,ScaleAdjY = 1.19,Explored = true},
[2034]={Name="Gilneas",7.335276,-650.7708,71.9306,Overlay="gilneas",Fish = 150,Explored = true},
--[2035]={Name="Gnomeregan",1.14238,-205.528,961.264,Overlay="gnomereganentrance",Fish = 150,City = true},
[2035]={Name="Gnomeregan",0,0,0,Overlay="gnomereganentrance",Fish = 150,City = true},
[2036]={Name="Lapidis Isle",4.330132,-905.624,2186.33848,Overlay="lapidis",Fish = 150,ScaleAdjY = 1.42,Explored = true},
[2037]={Name="Scarlet Monastery",0.40732,131.992,-589.474,Overlay="wetlands",Fish = 150,City = true},
[2038]={Name="The Deadmines",0.89978,-358.636,2210.988,Overlay="wetlands",Fish = 150,City = true},
[2039]={Name="Uldaman",1.12662,549.43,1190.682,Overlay="uldamanentrance",Fish = 150,City = true},
-- [2040]={Name="Copy EPG",8.0563,457.592712,-740.542767,Overlay="easternplaguelands",Fish = 425,}, 
[2040]={Name="Thalassian Highlands",6.164,201,-990.4,Overlay="thalassianhighlands",Fish = 425},
--
[90000]={1,0,0,},
[90001]={Name="Arathi Basin",3.508,0,-1600,Short = "AB",},
[90002]={Name="Warsong Gulch",2.29,0,-800,Short = "WG",},
[90003]={Name="Alterac Valley",8.471,0,0,Short = "AV",},
[90004]={Name="Eye of the Storm",4.538,0,800,Short = "EOS",},
[90005]={Name="Blade's Edge Arena",1,0,0,Short = "BEA",Arena = true},
[90006]={Name="Nagrand Arena",1,0,0,Short = "NA",Arena = true},
[90007]={Name="Ruins of Lordaeron",1,0,0,Short = "RL",Arena = true},
[90008]={Name="Strand of the Ancients",3.486,0,1600,Short = "SoA",},
[90009]={Name="Isle of Conquest",5.295,0,-2400,Short = "IC",},
-- [11024]={0,0,.02},
-- [11025]={0,0,.04},
-- [11147]={0,0,.06},
-- [12017]={0,.01,0},
-- [12061]={0,0,.01},
-- [13027]={0,.0,-.0},
-- [13028]={0,-.04,.0},
-- [13029]={0,-.02,.0},
-- [13030]={0,.02,.00},
}

--------

--Map.HotspotInfo = {
--}

--------
-- "Atlas\Images\Maps"


Map.AtlasInstanceInfo = {
	Atlas = 1,					-- Flag table as Atlas maps
	[103006] = {
		129 / 512, 386 / 512,		"AuchAuchenaiCrypts"
	},
	[103007] = {
		109 / 512, 44 / 512,		"AuchManaTombs",
	},
	[103008] = {
		458 / 512, 236 / 512,		"AuchSethekkHalls",
	},
	[103009] = {
		61 / 512, 77 / 512,		"AuchShadowLabyrinth", },
	[103013] = {
		104 / 512, 458 / 512,		"BlackTempleStart",
		104 / 512 - 1, 458 / 512,		"BlackTempleBasement",
		104 / 512, 458 / 512 + 1,		"BlackTempleTop",
	},
	[101014] = {
		171 / 512, 59 / 512,		"BlackfathomDeeps",
	},
	[102015] = {
		126 / 512, 420 / 512,		"BlackrockDepths",	},
	[102017] = {
		16 / 512, 71 / 512,		"BlackrockSpireLower",
		16 / 512, 71 / 512 + 1,		"BlackrockSpireUpper",
	},
	[102018] = {
		342 / 512, 361 / 512,		"BlackwingLair",
	},
	[101023] = {
		108 / 512, 252 / 512,		"CoTHyjal",
	},
	[101024] = {
		71 / 512, 206 / 512,		"CoTOldHillsbrad",
	},
	[101025] = {
		267 / 512, 135 / 512,		"CoTBlackMorass",
	},
	[103027] = {
		12 / 512, 339 / 512,		"CFRSerpentshrineCavern",
	},
	[103028] = {
		126 / 512, 119 / 512,		"CFRTheSlavePens",
	},
	[103029] = {
		14 / 512, 181 / 512,		"CFRTheSteamvault",
	},
	[103030] = {
		124 / 512, 341 / 512,		"CFRTheUnderbog",
	},
	[101036] = {
		385 / 512, 405 / 512,		"DireMaulNorth",
		385 / 512 + 1, 405 / 512,		"DireMaulWest",
		385 / 512 - 1, 405 / 512,		"DireMaulEast",
	},
	[102048] = {
		405 / 512, 73 / 512,		"Gnomeregan",
	},
	[103049] = {
		447 / 512, 364 / 512,	"GruulsLair",
	},
	[103051] = {
		213 / 512, 330 / 512,	"HCHellfireRamparts",
	},
	[103052] = {
		101 / 512, 81 / 512,		"HCMagtheridonsLair",
	},
	[103053] = {
		242 / 512, 473 / 512,	"HCBloodFurnace",
	},
	[103054] = {
		341 / 512, 497 / 512,	"HCTheShatteredHalls",
	},
	[102058] = {
		144 / 512, 217 / 512,		"KarazhanStart",
		144 / 512 - 1, 217 / 512,	"KarazhanEnd",
	},
	[101060] = {
		378 / 512, 63 / 512,		"Maraudon",
	},
	[102061] = {
		19 / 512, 114 / 512,		"MoltenCore",
	},
	[104065] = {
		210 / 512, 211 / 512,	"Naxxramas",
	},
	[101067] = {
		50 / 512, 66 / 512,		"OnyxiasLair",
	},
	[101069] = {
		379 / 512, 14 / 512,		"RagefireChasm",
	},
	[101070] = {
		26 / 512, 123 / 512,		"RazorfenDowns",
	},
	[101071] = {
		359 / 512, 361 / 512,	"RazorfenKraul",
	},
	[101073] = {
		320 / 512, 36 / 512,		"TheRuinsofAhnQiraj",
	},
	[102074] = {
		512 / 512, 512 / 512,	"SMArmory",
		0 / 512, 512 / 512,		"SMCathedral",
		512 / 512, 0 / 512,		"SMGraveyard",
		0 / 512, 0 / 512,			"SMLibrary",
	},
	[102075] = {
		124 / 512, 174 / 512,	"Scholomance",
	},
	[102077] = {
		373 / 512, 325 / 512,	"ShadowfangKeep",
	},
	[102086] = {
		266 / 512, 460 / 512,	"Stratholme",
	},
	[102087] = {
		307 / 512, 11 / 512,		"TheSunkenTemple",
	},
	[103091] = {
		193 / 512, 485 / 512,		"TempestKeepArcatraz",
	},
	[103092] = {
		494 / 512, 218 / 512,		"TempestKeepBotanica",
	},
	[103093] = {
		230 / 512, 482 / 512,		"TempestKeepTheEye",
	},
	[103094] = {
		219 / 512, 475 / 512,		"TempestKeepMechanar",
	},
	[101095] = {
		127 / 512, 193 / 512,		"TheTempleofAhnQiraj",
	},
	[102098] = {
		62 / 512, 100 / 512,		"TheDeadmines",
	},
	[102101] = {
		257 / 512, 347 / 512,		"TheStockade",
	},
	[102106] = {
		458 / 512, 379 / 512,		"Uldaman",
	},
	[101109] = {
		220 / 512, 298 / 512,		"WailingCaverns",
	},
	[102116] = {
		399 / 512, 453 / 512,		"ZulFarrak",
	},
	[102117] = {
		39 / 512, 259 / 512,		"ZulGurub",
	},
	[102118] = {
		39 / 512, 271 / 512,		"ZulAman",
	},
	[102120] = {
		209 / 512, 401 / 512,	"MagistersTerrace",
	},
	[102121] = {
		164 / 512, 69 / 512,		"SunwellPlateau",
	},
	[104133] = { 438 / 512, 349 / 512, "AhnKahet", },
	[104134] = { 108 / 512, 214 / 512, "AzjolNerub",	},
	[104135] = {  17 / 512, 279 / 512, "DrakTharonKeep", },
	[104136] = { 375 / 512, 172 / 512, "Gundrak", },
	[104137] = { 186 / 512, 459 / 512, "TheNexus", },
	[104138] = { 250 / 512, 244 / 512, "TheOculus", },
	[104139] = { 236 / 512, 481 / 512, "VioletHold", },
	[104140] = {   9 / 512, 154 / 512, "UlduarHallsofLightning", },
	[104141] = {  62 / 512, 209 / 512, "UlduarHallsofStone", },
	[104142] = { 334 / 512, 271 / 512, "UtgardeKeep", },
	[104144] = { 265 / 512, 257 / 512, "ObsidianSanctum", },
	[104145] = { 185 / 512,   9 / 512, "UtgardePinnacle", },
	[101147] = { 398 / 512, 489 / 512, "CoTOldStratholme", },
}

Map.InstanceInfo = {			-- Blizzard instance maps (SetInstanceMap uses size of 3 for table entries)

	[101014] = { 0, 0,			"BlackFathomDeeps\\BlackFathomDeeps",},
    [101192] = { 0, 0,			"CrescentGrove\\CrescentGrove", },
	[102015] = { 0, 0,			"BlackrockDepths\\BlackrockDepths",
					0, -100,		"BlackrockDepths2f\\BlackrockDepths2f", },
	[102017] = { 0, 0,			"BlackrockSpire\\BlackrockSpire",
					0, -100,		"BlackrockSpire2f\\BlackrockSpire2f",
					0, -200,		"BlackrockSpire3f\\BlackrockSpire3f",
					0, -300,		"BlackrockSpire4f\\BlackrockSpire4f",
					0, -400,		"BlackrockSpire5f\\BlackrockSpire5f",
					0, -500,		"BlackrockSpire6f\\BlackrockSpire6f",
					0, -600,		"BlackrockSpire7f\\BlackrockSpire7f", },
	[102018] = { 0, 0,			"BlackwingLair\\BlackwingLair",
					0, -100,		"BlackwingLair2f\\BlackwingLair2f",
					0, -200,		"BlackwingLair3f\\BlackwingLair3f",
					0, -300,		"BlackwingLair4f\\BlackwingLair4f", },
	[101036] = { 0, 0,			"DireMaul\\DireMaul",
					0, -100,		"DireMaul2f\\DireMaul2f",
					0, -200,		"DireMaul3f\\DireMaul3f",
					0, -300,		"DireMaul4f\\DireMaul4f",
					0, -400,		"DireMaul5f\\DireMaul5f",
					0, -500,		"DireMaul6f\\DireMaul6f", },
	[102048] = { 0, 0,			"Gnomeregan\\Gnomeregan",
					0, -100,		"Gnomeregan2f\\Gnomeregan2f",
					0, -200,		"Gnomeregan3f\\Gnomeregan3f",
					0, -300,		"Gnomeregan4f\\Gnomeregan4f", },
	[101060] = { 0, 0,			"Maraudon\\Maraudon", },
	[102061] = { 0, 0,			"MoltenCore\\MoltenCore", },
	[101069] = { 0, 0,			"Ragefire\\Ragefire", },
	[101070] = { 0, 0,			"RazorfenDowns\\RazorfenDowns", },
	[101071] = { 0, 0,			"RazorfenKraul\\RazorfenKraul", },
	[101073] = { 0, 0,			"RuinsofAhnQiraj\\RuinsofAhnQiraj", },
	[102074] = { 0, 0,			"ScarletMonastery\\ScarletMonastery",},
    [102171] = { 0, 0,			"ScarletMonastery2f\\ScarletMonastery2f",},
    [102172] = { 0, 0,			"ScarletMonastery3f\\ScarletMonastery3f",},
    [102173] = { 0, 0,			"ScarletMonastery4f\\ScarletMonastery4f",},
	[102075] = { 0, 0,			"Scholomance\\Scholomance",
					0, -100, 	"Scholomance2f\\Scholomance2f",
					0, -200,	"Scholomance3f\\Scholomance3f",
					0, -300, 	"Scholomance4f\\Scholomance4f", },
	[102077] = { 0, 0,			"ShadowfangKeep\\ShadowfangKeep",
					0, -100,		"ShadowfangKeep2f\\ShadowfangKeep2f",
					0, -200,		"ShadowfangKeep3f\\ShadowfangKeep3f",
					0, -300,		"ShadowfangKeep4f\\ShadowfangKeep4f",
					0, -400,		"ShadowfangKeep5f\\ShadowfangKeep5f",
					0, -500,		"ShadowfangKeep6f\\ShadowfangKeep6f",
					0, -600,		"ShadowfangKeep7f\\ShadowfangKeep7f", },
	[102086] = { 0, 0,			"Stratholme\\Stratholme", },
	[102098] = { 0, 0,			"TheDeadmines\\TheDeadmines",},
	[102101] = { 0, 0,			"TheStockade\\TheStockade", },
	[102087] = { 0, 0,			"TheTempleOfAtalHakkar\\TheTempleOfAtalHakkar", 
                    0, -100,		"TheTempleOfAtalHakkar2f\\TheTempleOfAtalHakkar2f",
                    0, -200,		"TheTempleOfAtalHakkar3f\\TheTempleOfAtalHakkar3f",},
	[102106] = { 0, 0,			"Uldaman\\Uldaman", },
	[101109] = { 0, 0,			"WailingCaverns\\WailingCaverns", },
	[102118] = { 0, 0,			"ZulAman\\ZulAman", },
	[101116] = { 0, 0,			"ZulFarrak\\ZulFarrak", },
    [102189] = { 0, 0,			"GilneasCity\\GilneasCity", },
	[102065] = { 0, 0,			"Naxxramas\\Naxxramas",
					0, -100,		"Naxxramas2f\\Naxxramas2f", },
	[101067] = { 0, 0,			"OnyxiasLair\\OnyxiasLair", },
	[102058] = { 0, 0,		    "Karazhan\\Karazhan", },
    [102051] = { 0, 0,		    "KarazhanCrypt\\KarazhanCrypt", },
    [102193] = { 0, 0,		    "HateforgeQuarry\\HateforgeQuarry", },
    [102194] = { 0, 0,		    "StormwindVault\\StormwindVault", },
}	

Map.ZoneOverlays = {
    ["hilsbrad"] = {
        ["tarrenmill"] = "509,0,220,310",
        ["southpointtower"] = "2,192,288,225",
        ["easternstrand"] = "524,339,230,320",
        ["azureloadmine"] = "175,275,165,200",
        ["purgationisle"] = "109,482,125,100",
        ["southshore"] = "418,201,235,270",
        ["hillsbradfields"] = "198,155,305,275",
        ["dungarok"] = "637,294,240,275",
        ["durnholdekeep"] = "605,75,384,365",
        ["westernstrand"] = "208,368,285,155",
        ["darrowhill"] = "414,154,205,155",
        ["nethanderstead"] = "541,236,215,240"
    },
    ["duskwood"] = {
        ["theyorgenfarmstead"] = "390,382,235,250",
        ["addlesstead"] = "55,342,275,250",
        ["tranquilgardenscemetary"] = "690,353,220,220",
        ["darkshire"] = "631,162,315,280",
        ["brightwoodgrove"] = "504,117,220,340",
        ["vulgologremound"] = "243,348,255,285",
        ["thehushedbank"] = "19,132,160,330",
        ["thedarkenedbank"] = "89,31,910,210",
        ["manormistmantle"] = "653,120,200,175",
        ["twilightgrove"] = "298,79,360,420",
        ["therottingorchard"] = "539,369,250,230",
        ["ravenhill"] = "102,302,195,145",
        ["ravenhillcemetary"] = "85,149,350,300"
    },
    ["hinterlands"] = {
        ["queldanillodge"] = "237,185,185,195",
        ["thealtarofzul"] = "373,365,200,165",
        ["shaolwatha"] = "571,239,280,205",
        ["thecreepingruin"] = "408,260,180,170",
        ["agolwatha"] = "374,164,205,195",
        ["plaguemistravine"] = "158,149,145,220",
        ["shadraalor"] = "240,387,195,185",
        ["aeriepeak"] = "13,245,255,205",
        ["valorwindlake"] = "319,302,170,170",
        ["jinthaalor"] = "505,333,235,285",
        ["skulkrock"] = "512,232,160,145",
        ["seradane"] = "509,19,275,275",
        ["hiriwatha"] = "171,306,225,200",
        ["theoverlookcliffs"] = "693,303,170,310"
    },
    ["blastedlands"] = {
        ["serpentscoil"] = "501,140,225,170",
        ["nethergardekeep"] = "559,30,185,190",
        ["dreadmaulpost"] = "361,195,245,195",
        ["altarofstorms"] = "310,133,185,155",
        ["riseofthedefiler"] = "405,123,170,145",
        ["darkportal"] = "453,259,265,220",
        ["dreadmaulhold"] = "361,15,195,180",
        ["garrisonarmory"] = "472,9,170,200",
        ["thetaintedscar"] = "212,178,384,450"
    },
    ["westfall"] = {
        ["thedaggerhills"] = "339,418,256,175",
        ["jangolodemine"] = "307,29,215,215",
        ["furlbrowspumpkinfarm"] = "387,11,210,215",
        ["goldcoastquarry"] = "220,102,225,256",
        ["themolsenfarm"] = "328,148,225,205",
        ["westfalllighthouse"] = "205,467,280,190",
        ["demontsplace"] = "208,375,200,185",
        ["alexstonfarmstead"] = "204,260,305,210",
        ["thedeadacre"] = "524,252,200,240",
        ["sentinelhill"] = "442,241,195,240",
        ["moonbrook"] = "317,331,220,200",
        ["thedustplains"] = "523,377,288,235",
        ["saldeansfarm"] = "459,105,225,210",
        ["thejansenstead"] = "488,0,165,200"
    },
    ["easternplaguelands"] = {
        ["zulmashar"] = "584,8,256,256",
        ["thefungalvale"] = "241,239,256,256",
        ["theundercroft"] = "142,455,256,191",
        ["pestilentscar"] = "590,269,256,288",
        ["lightshopechapel"] = "656,277,256,256",
        ["corinscrossing"] = "471,345,256,256",
        ["eastwalltower"] = "562,219,256,256",
        ["northpasstower"] = "427,87,256,256",
        ["scarletenclave"] = "718,218,284,450",
        ["thondrorilriver"] = "0,209,256,384",
        ["themarrisstead"] = "126,338,256,256",
        ["crownguardtower"] = "261,379,256,256",
        ["darrowshire"] = "279,467,256,179",
        ["blackwoodlake"] = "412,177,256,256",
        ["stratholme"] = "164,0,256,243",
        ["quellithienlodge"] = "392,14,256,256",
        ["plaguewood"] = "139,61,384,288",
        ["tyrshand"] = "687,449,256,197",
        ["thenoxiousglade"] = "692,144,256,256",
        ["theinfectisscar"] = "379,323,256,256",
        ["northdale"] = "590,106,256,256",
        ["lakemereldar"] = "474,412,256,205",
        ["terrordale"] = "49,76,256,256"
    },
    ["easternplaguelands"] = {
        ["thondrorilriver"] = "7,231,220,360",
        ["themarrisstead"] = "156,360,200,205",
        ["theundercroft"] = "172,477,185,150",
        ["crownguardtower"] = "291,401,205,165",
        ["thefungalvale"] = "271,261,210,210",
        ["darrowshire"] = "309,489,210,179",
        ["eastwalltower"] = "592,241,180,160",
        ["blackwoodlake"] = "442,199,230,235",
        ["northdale"] = "620,128,190,205",
        ["zulmashar"] = "614,30,205,165",
        ["northpasstower"] = "457,109,240,195",
        ["quellithienlodge"] = "422,36,230,150",
        ["terrordale"] = "79,98,190,205",
        ["plaguewood"] = "169,83,360,270",
        ["stratholme"] = "194,9,240,200",
        ["theinfectisscar"] = "620,291,195,275",
        ["corinscrossing"] = "537,367,165,160",
        ["lakemereldar"] = "537,463,250,175",
        ["tyrshand"] = "717,471,245,170",
        ["lightshopechapel"] = "716,299,175,245",
        ["pestilentscar"] = "409,345,205,250",
        ["thenoxiousglade"] = "722,166,225,215"
    },
    ["badlands"] = {
        ["agmondsend"] = "345,389,265,270",
        ["kargath"] = "0,148,256,256",
        ["apocryphansrest"] = "17,310,256,256",
        ["dustwindgulch"] = "498,209,245,205",
        ["campcagg"] = "12,428,256,256",
        ["campkosh"] = "551,48,220,220",
        ["lethlorravine"] = "611,110,370,455",
        ["mirageflats"] = "148,384,256,256",
        ["themakersterrace"] = "389,7,245,205",
        ["hammertoesdigsite"] = "445,120,200,195",
        ["thedustbowl"] = "159,199,270,275",
        ["angorfortress"] = "325,148,195,200",
        ["valleyoffangs"] = "349,256,230,230",
        ["campboff"] = "501,341,255,280"
    },
    ["silverpine"] = {
        ["shadowfangkeep"] = "364,359,220,160",
        ["maldensorchard"] = "465,0,256,160",
        ["thedeadfield"] = "402,65,175,165",
        ["deepelemmine"] = "470,261,160,170",
        ["pyrewoodvillage"] = "391,446,140,125",
        ["berensperil"] = "491,417,240,180",
        ["theshiningstrand"] = "459,13,256,220",
        ["thedecrepitferry"] = "457,144,180,185",
        ["ambermill"] = "494,262,240,240",
        ["olsensfarthing"] = "382,252,165,185",
        ["thesepulcher"] = "352,168,210,160",
        ["thegreymanewall"] = "379,447,210,215",
        ["fenrisisle"] = "593,74,250,215",
        ["northtideshollow"] = "323,128,180,128",
        ["theskitteringdark"] = "286,37,185,165"
    },
    ["wetlands"] = {
        ["TURTLEwetlands5"] = "0,256,256,256,1",
        ["TURTLEwetlands6"] = "256,256,256,256,1",
        ["sundownmarsh"] = "92,82,300,240",
        ["blackchannelmarsh"] = "77,245,240,175",
        ["mosshidefen"] = "527,264,205,245",
        ["saltsprayglen"] = "237,41,200,240",
        ["direforgehill"] = "507,115,256,250",
        ["raptorridge"] = "628,176,190,160",
        ["grimbatol"] = "611,230,350,360",
        ["bluegillmarsh"] = "89,142,225,190",
        ["thegreenbelt"] = "456,125,185,240",
        ["thelganrock"] = "470,371,230,190",
        ["angerfangencampment"] = "347,218,225,185",
        ["whelgarsexcavationsite"] = "247,205,195,185",
        ["dunmodr"] = "401,21,205,180",
        ["ironbeardstomb"] = "349,115,200,185",
        ["menethilharbor"] = "13,314,175,128"
    },
    ["deadwindpass"] = {
        ["deadmanscrossing"] = "249,76,380,365",
        ["thevice"] = "426,299,270,270",
        ["karazhan"] = "269,337,300,245"
    },
    ["searinggorge"] = {
        ["grimsiltdigsite"] = "494,300,305,220",
        ["tannercamp"] = "545,407,305,230",
        ["dustfirevalley"] = "422,8,460,365",
        ["blackcharcave"] = "77,366,275,235",
        ["theseaofcinders"] = "247,388,360,280",
        ["thecauldron"] = "250,170,425,325",
        ["firewatchridge"] = "85,30,405,430"
    },
    ["elwynn"] = {
        ["fargodeepmine"] = "238,428,256,240",
        ["northshirevalley"] = "381,147,256,256",
        ["goldshire"] = "250,270,240,220",
        ["stonecairnlake"] = "587,190,310,256",
        ["crystallake"] = "422,332,225,220",
        ["stormwind"] = "0,0,485,405",
        ["ridgepointtower"] = "696,435,306,233",
        ["towerofazora"] = "551,292,255,250",
        ["eastvaleloggingcamp"] = "704,330,256,210",
        ["brackwellpumpkinpatch"] = "577,419,256,249",
        ["forestsedge"] = "124,327,256,341",
        ["jerodslanding"] = "425,431,256,237"
    },
    ["arathi"] = {
        ["TURTLEarathi2"] = "256,0,256,256,1",
        ["TURTLEarathi3"] = "512,0,256,256,1",
        ["refugepoint"] = "370,186,175,225",
        ["hammerfall"] = "655,120,205,250",
        ["northfoldmanor"] = "192,90,230,240",
        ["circleofeastbinding"] = "558,112,160,230",
        ["bouldergor"] = "232,145,245,245",
        ["goshekfarm"] = "531,276,230,195",
        ["boulderfisthall"] = "432,362,215,235",
        ["thoradinswall"] = "87,138,190,240",
        ["thandolspan"] = "355,412,200,220",
        ["witherbarkvillage"] = "559,333,215,210",
        ["faldirscove"] = "171,424,256,215",
        ["circleofouterbinding"] = "419,293,170,155",
        ["stromgardekeep"] = "108,287,240,230",
        ["dabyriesfarmstead"] = "472,165,180,210",
        ["circleofinnerbinding"] = "286,310,210,185",
        ["circleofwestbinding"] = "138,54,190,210"
    },
    ["dunmorogh"] = {
        ["TURTLEdunmorogh1"] = "0,0,256,256,1",
        ["TURTLEdunmorogh2"] = "256,0,256,256,1",
        ["TURTLEdunmorogh3"] = "512,0,256,256,1",
        ["TURTLEdunmorogh4"] = "768,0,256,256,1",
        ["TURTLEdunmorogh5"] = "0,256,256,256,1",
        ["TURTLEdunmorogh7"] = "512,256,256,256,1",
        ["TURTLEdunmorogh8"] = "768,256,256,256,1",
        ["chillbreezevalley"] = "274,296,180,128",
        ["gnomeragon"] = "166,184,180,165",
        ["southerngateoutpost"] = "792,279,128,120",
        ["amberstillranch"] = "573,280,128,128",
        ["mistypinerefuge"] = "502,221,128,165",
        ["frostmanehold"] = "217,287,125,125",
        ["ironforge"] = "397,163,315,200",
        ["thegrizzledden"] = "314,311,200,185",
        ["coldridgepass"] = "295,385,150,128",
        ["brewnallvillage"] = "252,249,115,115",
        ["shimmerridge"] = "347,163,128,190",
        ["golbolarquarry"] = "608,291,165,165",
        ["iceflowlake"] = "281,167,128,180",
        ["anvilmar"] = "155,403,240,185",
        ["northerngateoutpost"] = "759,173,128,165",
        ["helmsbedlake"] = "694,273,155,170",
        ["thetundridhills"] = "522,322,155,128",
        ["kharanos"] = "386,294,200,200"
    },
    ["burningsteppes"] = {
        ["blackrockpass"] = "589,279,270,310",
        ["blackrockstronghold"] = "334,114,245,265",
        ["pillarofash"] = "377,285,320,270",
        ["altarofstorms"] = "36,109,225,220",
        ["ruinsofthaurissan"] = "513,99,270,285",
        ["blackrockmountain"] = "173,101,256,280",
        ["terrorwingpath"] = "722,46,280,355",
        ["dreadmaulrock"] = "707,168,220,225",
        ["morgansvigil"] = "708,311,294,270",
        ["dracodar"] = "56,258,415,315"
    },
    ["westernplaguelands"] = {
        ["thebulwark"] = "137,293,225,185",
        ["hearthglen"] = "307,16,340,288",
        ["caerdarrow"] = "600,412,170,165",
        ["sorrowhill"] = "355,462,300,206",
        ["felstonefield"] = "300,311,160,125",
        ["darrowmerelake"] = "504,343,370,270",
        ["northridgelumbercamp"] = "382,164,220,180",
        ["thewrithinghaunt"] = "451,323,170,190",
        ["thondrorilriver"] = "590,86,205,340",
        ["ruinsofandorhol"] = "260,355,285,230",
        ["dalsonstears"] = "381,265,220,150",
        ["theweepingcave"] = "566,198,160,200",
        ["gahrronswithering"] = "520,250,180,205"
    },
    ["tirisfal"] = {
        ["TURTLEtirisfal5"] = "0,256,256,256,1",
        ["TURTLEtirisfal6"] = "256,256,256,256,1",
        ["TURTLEtirisfal9"] = "0,512,256,256,1",
        ["TURTLEtirisfal10"] = "256,512,256,256,1",
        ["balnirfarmstead"] = "630,326,216,179",
        ["crusaderoutpost"] = "694,289,173,128",
        ["nightmarevale"] = "363,349,243,199",
        ["brill"] = "537,299,128,256",
        ["sollidenfarmstead"] = "239,250,256,156",
        ["scarletwatchpost"] = "689,104,175,247",
        ["agamandmills"] = "335,139,256,210",
        ["brightwaterlake"] = "587,139,201,288",
        ["ruinsoflordaeron"] = "463,361,315,235",
        ["bulwark"] = "698,362,230,205",
        ["stillwaterpond"] = "395,277,186,128",
        ["coldhearthmanor"] = "474,327,150,128",
        ["deathknell"] = "227,328,245,205",
        ["monastary"] = "746,125,211,189",
        ["venomwebvale"] = "757,205,237,214",
        ["garrenshaunt"] = "497,145,174,220"
    },
    ["redridge"] = {
        ["rendersvalley"] = "484,361,465,255",
        ["lakeridgehighway"] = "187,333,430,290",
        ["renderscamp"] = "277,0,275,256",
        ["lakeeverstill"] = "133,240,535,275",
        ["lakeshire"] = "83,197,340,195",
        ["althersmill"] = "399,129,235,270",
        ["stonewatchfalls"] = "595,320,320,210",
        ["stonewatch"] = "500,215,255,300",
        ["galardellvalley"] = "654,161,250,250",
        ["threecorners"] = "0,284,365,350",
        ["redridgecanyons"] = "121,72,365,245"
    },
    ["swampofsorrows"] = {
        ["splinterspearjunction"] = "129,236,275,240",
        ["stagalbog"] = "552,378,345,250",
        ["stonard"] = "279,237,360,315",
        ["theshiftingmire"] = "286,110,315,235",
        ["sorrowmurk"] = "724,120,215,365",
        ["ithariusscave"] = "0,262,240,245",
        ["pooloftears"] = "565,218,300,275",
        ["fallowsanctuary"] = "492,0,365,305",
        ["mistyvalley"] = "0,140,245,305",
        ["theharborage"] = "171,145,235,205",
        ["mistyreedstrand"] = "746,0,256,668"
    },
    ["lochmodan"] = {
        ["thefarstriderlodge"] = "546,199,370,295",
        ["stonewroughtdam"] = "339,11,290,175",
        ["silverstreammine"] = "229,11,235,270",
        ["northgatepass"] = "125,12,230,300",
        ["ironbandsexcavationsite"] = "482,321,345,256",
        ["stonesplintervalley"] = "215,348,255,285",
        ["thelsamar"] = "217,203,256,230",
        ["grizzlepawridge"] = "309,310,295,358",
        ["valleyofkings"] = "109,370,195,250",
        ["mogroshstronghold"] = "542,48,315,235",
        ["theloch"] = "352,87,320,410"
    },
    ["stranglethorn"] = {
        ["bootybay"] = "203,433,145,128",
        ["ruinsofjubuwal"] = "306,301,110,110",
        ["balalruins"] = "241,92,90,80",
        ["mizjahruins"] = "311,131,105,110",
        ["wildshore"] = "229,422,165,190",
        ["moshoggogremound"] = "432,94,128,175",
        ["ruinsofaboraz"] = "350,335,95,95",
        ["thearena"] = "235,189,200,185",
        ["lakenazferiti"] = "331,59,128,125",
        ["kurzenscompound"] = "388,0,155,150",
        ["rebelcamp"] = "284,0,170,90",
        ["thevilereef"] = "152,90,190,175",
        ["ziatajairuins"] = "364,231,128,125",
        ["ruinsofzulkunda"] = "196,3,125,140",
        ["zuuldaiaruins"] = "156,42,115,115",
        ["bloodsailcompound"] = "194,284,165,175",
        ["crystalveinmine"] = "345,276,120,120",
        ["nesingwarysexpedition"] = "269,26,140,110",
        ["kalairuins"] = "299,88,95,95",
        ["nekmaniwellspring"] = "211,359,90,115",
        ["zulgurub"] = "483,8,245,220",
        ["baliamahruins"] = "371,129,110,140",
        ["ruinsofzulmamwe"] = "394,212,170,125",
        ["venturecobasecamp"] = "387,64,105,125",
        ["mistvalevalley"] = "280,368,125,125",
        ["gromgolbasecamp"] = "260,132,110,105",
        ["jagueroisle"] = "314,493,125,120"
    },
    ["alterac"] = {
        ["gallowscorner"] = "406,279,200,200",
        ["gavinsnaze"] = "225,478,160,175",
        ["lordamereinternmentcamp"] = "44,403,330,265",
        ["mistyshore"] = "196,131,220,280",
        ["dandredsfold"] = "276,0,285,230",
        ["soferasnaze"] = "462,307,255,320",
        ["crushridgehold"] = "334,162,280,240",
        ["corrahnsdagger"] = "399,380,195,288",
        ["theheadland"] = "314,471,165,197",
        ["strahnbrad"] = "549,105,370,300",
        ["ruinsofalterac"] = "270,197,255,255",
        ["growlesscave"] = "317,372,190,170",
        ["chillwindpoint"] = "626,253,350,370",
        ["theuplands"] = "462,77,235,200",
        ["dalaran"] = "26,262,300,300"
    },
    ["ashenvale"] = {
        ["astranaar"] = "272,251,205,185",
        ["boughshadow"] = "856,151,146,200",
        ["fallenskylake"] = "547,426,235,205",
        ["felfirehill"] = "713,344,245,255",
        ["firescarshrine"] = "189,324,165,175",
        ["irislake"] = "392,218,200,205",
        ["lakefalathim"] = "131,137,128,195",
        ["maestraspost"] = "205,38,215,305",
        ["mystrallake"] = "356,347,275,240",
        ["nightrun"] = "597,258,225,255",
        ["raynewoodretreat"] = "520,238,180,245",
        ["satyrnaar"] = "694,225,285,185",
        ["thehowlingvale"] = "463,141,210,185",
        ["theruinsofstardust"] = "260,373,155,150",
        ["theshrineofaessina"] = "104,259,220,195",
        ["thezoramstrand"] = "19,28,245,245",
        ["thistlefurvillage"] = "203,158,255,195",
        ["warsonglumbercamp"] = "796,311,200,160"
    },
    ["aszhara"] = {
        ["bayofstorms"] = "479,201,270,300",
        ["bitterreaches"] = "644,40,245,185",
        ["forlornridge"] = "191,369,220,255",
        ["haldarrencampment"] = "77,331,200,150",
        ["jaggedreef"] = "366,0,570,170",
        ["lakemennar"] = "296,429,315,200",
        ["legashencampment"] = "478,44,235,140",
        ["ravencrestmonument"] = "552,499,240,125",
        ["ruinsofeldarath"] = "238,221,265,280",
        ["shadowsongshrine"] = "35,422,225,180",
        ["southridgebeach"] = "389,353,370,220",
        ["templeofarkkoran"] = "681,153,190,200",
        ["thalassianbasecamp"] = "499,119,240,155",
        ["theruinedreaches"] = "396,540,395,128",
        ["theshatteredstrand"] = "404,194,160,210",
        ["timbermawhold"] = "250,106,235,270",
        ["towerofeldara"] = "818,107,120,155",
        ["ursolan"] = "422,95,145,215",
        ["valormok"] = "84,229,215,175"
    },
    ["barrens"] = {
        ["agamagor"] = "340,234,200,185",
        ["baelmodan"] = "431,479,128,128",
        ["blackthornridge"] = "335,462,155,128",
        ["boulderlodemine"] = "555,0,120,110",
        ["bramblescar"] = "442,298,125,165",
        ["camptaurajo"] = "365,350,145,125",
        ["dreadmistpeak"] = "419,63,128,105",
        ["farwatchpost"] = "564,52,100,165",
        ["fieldofgiants"] = "355,402,210,150",
        ["groldomfarm"] = "492,63,125,115",
        ["honorsstand"] = "306,130,128,128",
        ["lushwateroasis"] = "365,177,175,185",
        ["northwatchfold"] = "527,307,150,120",
        ["raptorgrounds"] = "507,294,115,110",
        ["ratchet"] = "556,189,125,125",
        ["razorfendowns"] = "407,553,155,115",
        ["razorfenkraul"] = "341,537,128,128",
        ["thecrossroads"] = "431,118,155,155",
        ["thedryhills"] = "317,29,200,145",
        ["theforgottenpools"] = "384,115,120,125",
        ["themerchantcoast"] = "581,247,95,100",
        ["themorshanrampart"] = "412,0,128,100",
        ["thesludgefen"] = "456,0,170,120",
        ["thestagnantoasis"] = "481,211,155,128",
        ["thornhill"] = "498,119,140,128"
    },
    ["darkshore"] = {
        ["ametharan"] = "324,306,190,205",
        ["auberdine"] = "318,162,150,215",
        ["bashalaran"] = "365,181,180,195",
        ["cliffspringriver"] = "375,94,230,190",
        ["groveoftheancients"] = "305,412,200,170",
        ["remtravelsexcavation"] = "229,485,175,183",
        ["ruinsofmathystra"] = "510,0,195,215",
        ["themastersglaive"] = "329,510,175,158",
        ["towerofalthalaxx"] = "468,85,170,195"
    },
    ["desolace"] = {
        ["ethelrethor"] = "311,61,205,250",
        ["gelkisvillage"] = "293,426,195,242",
        ["kodograveyard"] = "387,244,275,250",
        ["kolkarvillage"] = "607,215,220,220",
        ["kormekshut"] = "555,181,170,160",
        ["magramvillage"] = "590,365,205,285",
        ["mannoroccoven"] = "399,380,285,280",
        ["nijelspoint"] = "554,0,200,250",
        ["ranazjarisle"] = "241,6,100,100",
        ["sargeron"] = "625,33,285,245",
        ["shadowbreakravine"] = "690,444,205,195",
        ["shadowpreyvillage"] = "167,389,230,230",
        ["tethrisaran"] = "431,0,205,145",
        ["thunderaxefortress"] = "447,102,190,220",
        ["valleyofspears"] = "212,215,245,285"
    },
    ["durotar"] = {
        ["drygulchravine"] = "427,78,210,160",
        ["echoisles"] = "549,427,200,240",
        ["kolkarcrag"] = "413,476,160,120",
        ["orgrimmar"] = "244,0,445,160",
        ["razorhill"] = "432,170,220,230",
        ["razormanegrounds"] = "301,189,230,230",
        ["senjinvillage"] = "474,384,160,190",
        ["skullrock"] = "464,33,128,110",
        ["thunderridge"] = "327,60,190,200",
        ["tiragardekeep"] = "462,286,190,180",
        ["valleyoftrials"] = "355,320,215,215"
    },
    ["dustwallow"] = {
        ["alcazisland"] = "660,21,200,195",
        ["backbaywetlands"] = "239,189,400,255",
        ["brackenwallvillage"] = "230,0,280,270",
        ["thedenofflame"] = "257,313,255,250",
        ["theramoreisle"] = "534,224,230,205",
        ["thewyrmbog"] = "367,381,285,240",
        ["witchhill"] = "422,0,250,315"
    },
    ["felwood"] = {
        ["bloodvenomfalls"] = "292,263,235,145",
        ["deadwoodvillage"] = "408,533,175,135",
        ["emeraldsanctuary"] = "405,429,185,160",
        ["felpawvillage"] = "483,0,240,145",
        ["irontreewoods"] = "420,54,215,215",
        ["jadefireglen"] = "332,465,165,155",
        ["jadefirerun"] = "330,29,195,170",
        ["jaedenar"] = "271,331,245,128",
        ["morlosaran"] = "496,509,145,159",
        ["ruinsofconstellas"] = "297,381,235,155",
        ["shatterscarvale"] = "307,123,235,200",
        ["talonbranchglade"] = "548,90,160,145"
    },
    ["feralas"] = {
        ["TURTLEferalas8"] = "768,256,256,256,1",
        ["campmojache"] = "689,233,155,160",
        ["diremaul"] = "454,201,230,195",
        ["dreambough"] = "454,0,150,125",
        ["feralscarvale"] = "486,329,115,115",
        ["frayfeatherhighlands"] = "478,386,110,170",
        ["gordunnioutpost"] = "690,141,140,165",
        ["grimtotemcompound"] = "623,167,120,195",
        ["isleofdread"] = "192,375,215,293",
        ["lowerwilds"] = "751,198,225,180",
        ["oneiros"] = "493,70,110,110",
        ["ruinsofisildien"] = "540,320,190,250",
        ["ruinsofravenwind"] = "305,0,190,155",
        ["sardorisle"] = "208,234,180,180",
        ["theforgottencoast"] = "404,256,145,320",
        ["thetwincolossals"] = "319,75,285,245",
        ["thewrithingdeep"] = "618,298,240,220"
    },
    ["moonglade"] = {
		["lakeeluneara"] = "244,89,555,510"
	},
    ["mulgore"] = {
        ["baeldundigsite"] = "255,214,210,180",
        ["bloodhoofvillage"] = "367,303,256,200",
        ["palemanerock"] = "303,307,128,205",
        ["ravagedcaravan"] = "473,260,128,120",
        ["redcloudmesa"] = "270,425,470,243",
        ["redrocks"] = "502,16,205,230",
        ["thegoldenplains"] = "428,80,215,240",
        ["therollingplains"] = "523,356,256,190",
        ["theventurecomine"] = "532,238,225,235",
        ["thunderbluff"] = "249,59,280,240",
        ["thunderhornwaterwell"] = "379,242,128,155",
        ["wildmanewaterwell"] = "291,0,185,128",
        ["windfuryridge"] = "395,0,205,128",
        ["winterhoofwaterwell"] = "458,369,170,128"
    },
    ["silithus"] = {
        ["hiveashi"] = "265,12,512,320",
        ["hiveregal"] = "245,285,512,384",
        ["hivezora"] = "97,144,384,512",
        ["southwindvillage"] = "500,65,384,384",
        ["thecrystalvale"] = "104,24,320,289",
        ["thescarabwall"] = "116,413,288,256",
        ["twilightbasecamp"] = "344,197,320,256"
    },
    ["stonetalonmountains"] = {
        ["TURTLEstonetalonmountains1"] = "0,0,256,256,1",
        ["TURTLEstonetalonmountains2"] = "256,0,256,256,1",
        ["TURTLEstonetalonmountains5"] = "0,256,256,256,1",
        ["TURTLEstonetalonmountains6"] = "256,256,256,256,1",
        ["TURTLEstonetalonmountains7"] = "512,256,256,256,1",
        ["TURTLEstonetalonmountains8"] = "768,256,256,256,1",
        ["TURTLEstonetalonmountains11"] = "512,512,256,256,1",
        ["TURTLEstonetalonmountains12"] = "768,512,256,256,1",
        ["boulderslideravine"] = "572,561,145,107",
        ["campaparaje"] = "718,571,190,97",
        ["grimtotempost"] = "668,515,225,120",
        ["malakajin"] = "663,582,125,86",
        ["mirkfallonlake"] = "390,145,200,215",
        ["sishircanyon"] = "475,433,125,125",
        ["stonetalonpeak"] = "247,0,270,205",
        ["sunrockretreat"] = "389,320,150,150",
        ["thecharredvale"] = "210,234,230,355",
        ["webwinderpath"] = "457,282,288,355",
        ["windshearcrag"] = "553,197,320,275"
    },
    ["tanaris"] = {
        ["abyssalsands"] = "363,194,215,180",
        ["brokenpillar"] = "473,234,110,180",
        ["cavernsoftime"] = "561,256,155,150",
        ["dunemaulcompound"] = "325,289,205,145",
        ["eastmoonruins"] = "395,346,160,150",
        ["gadgetzan"] = "421,91,175,165",
        ["landsendbeach"] = "445,511,205,157",
        ["lostriggercove"] = "629,220,160,190",
        ["noonshaderuins"] = "533,104,120,135",
        ["sandsorrowwatch"] = "299,100,195,175",
        ["southbreakshore"] = "499,293,215,175",
        ["southmoonruins"] = "323,359,195,210",
        ["steamwheedleport"] = "592,75,155,150",
        ["thegapingchasm"] = "449,372,220,210",
        ["thenoxiouslair"] = "252,199,180,200",
        ["thistleshrubvalley"] = "203,286,185,250",
        ["valleyofthewatchers"] = "291,434,150,160",
        ["waterspringfield"] = "509,168,165,180",
        ["zalashjisden"] = "611,147,110,140",
        ["zulfarrak"] = "254,0,210,175"
    },
    ["teldrassil"] = {
        ["banethilhollow"] = "382,281,160,210",
        ["darnassus"] = "101,247,315,256",
        ["dolanaar"] = "462,323,190,128",
        ["gnarlpinehold"] = "368,443,185,128",
        ["lakealameth"] = "436,380,256,185",
        ["poolsofarlithrien"] = "335,313,128,190",
        ["ruttheranvillage"] = "494,548,128,100",
        ["shadowglen"] = "491,153,225,225",
        ["starbreezevillage"] = "561,292,200,200",
        ["theoracleglade"] = "272,127,170,240",
        ["wellspringlake"] = "377,93,180,256"
    },
    ["thousandneedles"] = {
        ["campethok"] = "0,0,305,310",
        ["darkcloudpinnacle"] = "259,131,205,195",
        ["freewindpost"] = "357,264,210,190",
        ["highperch"] = "31,155,190,190",
        ["splithoofcrag"] = "391,192,210,195",
        ["thegreatlift"] = "205,70,210,180",
        ["thescreechingcanyon"] = "179,200,250,240",
        ["theshimmeringflats"] = "610,300,320,365",
        ["windbreakcanyon"] = "492,250,240,220"
    },
    ["ungorocrater"] = {
        ["fireplumeridge"] = "367,178,295,270",
        ["golakkahotsprings"] = "121,151,315,345",
        ["ironstoneplateau"] = "582,67,285,285",
        ["lakkaritarpits"] = "160,6,570,265",
        ["terrorrun"] = "158,368,345,285",
        ["themarshlands"] = "560,240,310,355",
        ["theslitheringscar"] = "367,380,345,285"
    },
    ["winterspring"] = {
        ["darkwhispergorge"] = "447,441,255,205",
        ["everlook"] = "509,107,165,200",
        ["frostfirehotsprings"] = "222,172,240,140",
        ["frostsaberrock"] = "368,7,250,180",
        ["frostwhispergorge"] = "523,376,200,160",
        ["icethistlehills"] = "611,242,125,165",
        ["lakekeltheril"] = "401,198,215,185",
        ["mazthoril"] = "493,258,185,180",
        ["owlwingthicket"] = "593,340,165,140",
        ["starfallvillage"] = "392,137,185,160",
        ["thehiddengrove"] = "555,27,175,185",
        ["timbermawpost"] = "229,243,230,120",
        ["winterfallvillage"] = "617,158,145,125"
    },
    ["gilneas"] = {
        ["TURTLEgilneas1"] = "0,0,256,256,1",
        ["TURTLEgilneas2"] = "256,0,256,256,1",
        ["TURTLEgilneas3"] = "512,0,256,256,1",
        ["TURTLEgilneas5"] = "0,256,256,256,1",
        ["TURTLEgilneas6"] = "256,256,256,256,1",
        ["TURTLEgilneas7"] = "512,256,256,256,1",
        ["TURTLEgilneas9"] = "0,512,256,256,1",
        ["TURTLEgilneas10"] = "256,512,256,256,1",
        ["TURTLEgilneas11"] = "512,512,256,256,1"
    },
    ["telabim"] = {
        ["TURTLEtelabim2"] = "256,0,256,256,1",
        ["TURTLEtelabim3"] = "512,0,256,256,1",
        ["TURTLEtelabim6"] = "256,256,256,256,1",
        ["TURTLEtelabim7"] = "512,256,256,256,1",
        ["TURTLEtelabim10"] = "256,512,256,256,1",
        ["TURTLEtelabim11"] = "512,512,256,256,1"
    },
    ["hyjal"] = {["TURTLEhyjal"] = "0,0,1024,768"},
    ["icepoint"] = {["icepoint"] = "0,0,1024,768"},
    ["amanialor"] = {
        ["TURTLEamanialor1"] = "0,0,256,256,1",
        ["TURTLEamanialor2"] = "256,0,256,256,1",
        ["TURTLEamanialor3"] = "512,0,256,256,1",
        ["TURTLEamanialor4"] = "768,0,256,256,1",
        ["TURTLEamanialor5"] = "0,256,256,256,1",
        ["TURTLEamanialor6"] = "256,256,256,256,1",
        ["TURTLEamanialor7"] = "512,256,256,256,1",
        ["TURTLEamanialor8"] = "768,256,256,256,1",
        ["TURTLEamanialor10"] = "256,512,256,256,1",
        ["TURTLEamanialor11"] = "512,512,256,256,1",
        ["TURTLEamanialor12"] = "768,512,256,256,1",
    },
    ["gillijim"] = {["TURTLEgillijim"] = "0,0,1024,768"},
    ["lapidis"] = {
        ["TURTLElapidis1"] = "0,0,256,256,1",
        ["TURTLElapidis2"] = "256,0,256,256,1",
        ["TURTLElapidis3"] = "512,0,256,256,1",
        ["TURTLElapidis4"] = "768,0,256,256,1",
        ["TURTLElapidis5"] = "0,256,256,256,1",
        ["TURTLElapidis6"] = "256,256,256,256,1",
        ["TURTLElapidis7"] = "512,256,256,256,1",
        ["TURTLElapidis8"] = "768,256,256,256,1",
        ["TURTLElapidis9"] = "0,512,256,256,1",
        ["TURTLElapidis10"] = "256,512,256,256,1",
        ["TURTLElapidis11"] = "512,512,256,256,1",
    },
    ["scarletenclave"] = {
        ["TURTLEscarletenclave2"] = "256,0,256,256,1",
        ["TURTLEscarletenclave3"] = "512,0,256,256,1",
        ["TURTLEscarletenclave6"] = "256,256,256,256,1",
        ["TURTLEscarletenclave7"] = "512,256,256,256,1",
        ["TURTLEscarletenclave10"] = "256,512,256,256,1",
        ["TURTLEscarletenclave11"] = "512,512,256,256,1",
    },
    ["gmisland"] = {["gmisland"] = "0,0,1024,768"},
    ["thalassianhighlands"] = {
        ["alahthalas"] = "256,0,768,512",
        ["ruinsofnashalaran"] = "0,0,512,512",
        ["isleofeternalautumn"] = "0,0,512,512",
        ["thefarstride"] = "0,0,512,512",
        ["anasterianpark"] = "256,0,512,512",
        ["thelastrunestone"] = "256,256,512,512",
        ["silversunmine"] = "256,256,256,512",
        ["brinthilien"] = "256,256,512,512",
        ["felstriderretreat"] = "0,256,512,512",
    },
    ["blackstoneisland"] = {
        ["gazziksworkshop"] = "256,256,512,512",
        ["rustgateridge"] = "256,256,512,512",
        ["blackashcoalpits"] = "0,0,512,768",
        ["blackashmine"] = "256,0,512,512",
        ["rustgatelumberyard"] = "512,0,256,512",
        ["venturecoslums"] = "256,0,512,512",
        ["thewaterhole"] = "256,0,512,512",
    },
    ["uldamanentrance"] = {["uldamanentrance"] = "0,0,1024,768"},
}
-- convert from dbc (\w+)\s(\d+)\s(\d+)\s(\d+)\s(\d+) -> ["\L$1"] = "$4,$5,$2,$3",

-- magick input.png +repage -crop 256x256 -scene 1 Bouldergor%d.png
-- for x in *.blp; do BLPConverter $x; done

--------
Map.EMB={[2352]="67ba43d493e62a8fad5de319e6d4cb05",[2353]="67ba43d493e62a8fad5de319e6d4cb05",[2354]="67ba43d493e62a8fad5de319e6d4cb05",[2355]="67ba43d493e62a8fad5de319e6d4cb05",[2356]="67ba43d493e62a8fad5de319e6d4cb05",[2452]="67ba43d493e62a8fad5de319e6d4cb05",[2453]="888f206847be6544f0061aadb4cefb3e",[2454]="2ca5c78ea6b9c5048fd8cb533518ad9a",[2455]="f2a1df5bd8f7d60a50459493071eb47d",[2456]="cccfaafb0e11dbf01d154ab1d4abbe27",[2457]="233df44e307aacc186d8e04ed159bf14",[2458]="63f50db145c3395b1caf29a89965b67c",[2459]="67ba43d493e62a8fad5de319e6d4cb05",[2522]="67ba43d493e62a8fad5de319e6d4cb05",[2523]="67ba43d493e62a8fad5de319e6d4cb05",[2524]="67ba43d493e62a8fad5de319e6d4cb05",[2525]="67ba43d493e62a8fad5de319e6d4cb05",[2526]="67ba43d493e62a8fad5de319e6d4cb05",[2527]="67ba43d493e62a8fad5de319e6d4cb05",[2528]="67ba43d493e62a8fad5de319e6d4cb05",[2529]="67ba43d493e62a8fad5de319e6d4cb05",[2530]="67ba43d493e62a8fad5de319e6d4cb05",[2531]="67ba43d493e62a8fad5de319e6d4cb05",[2532]="67ba43d493e62a8fad5de319e6d4cb05",[2533]="67ba43d493e62a8fad5de319e6d4cb05",[2534]="67ba43d493e62a8fad5de319e6d4cb05",[2535]="67ba43d493e62a8fad5de319e6d4cb05",[2536]="600e88d5c11e57a967a7b84c50af407f",[2547]="67ba43d493e62a8fad5de319e6d4cb05",[2548]="67ba43d493e62a8fad5de319e6d4cb05",[2549]="67ba43d493e62a8fad5de319e6d4cb05",[2550]="67ba43d493e62a8fad5de319e6d4cb05",[2551]="67ba43d493e62a8fad5de319e6d4cb05",[2552]="1a710f45539968166d1a1d1be18727d6",[2553]="f11a7c3f915d9813c7228b5abfe46f5f",[2554]="0492961947ef90285317f67f0da4b2cc",[2555]="d797d7e9f0d5a251917346d282c21987",[2556]="002f8ccd660bd4688ab989528738c324",[2557]="be91150301f9576783766625996a51c3",[2558]="34f60da6b3910ae83fac257f82be4a97",[2559]="4c1ca18d8f4363b82841db56c35e2d8a",[2622]="67ba43d493e62a8fad5de319e6d4cb05",[2623]="67ba43d493e62a8fad5de319e6d4cb05",[2624]="67ba43d493e62a8fad5de319e6d4cb05",[2625]="67ba43d493e62a8fad5de319e6d4cb05",[2626]="67ba43d493e62a8fad5de319e6d4cb05",[2627]="0bb5381ea9d1030d9b905dd8a5b8c858",[2628]="5f18cb4eb1e6b62a645df47414fceed0",[2629]="fa32ced4f63ac357919108b302aaffcb",[2630]="e0ab4a77166e766cfea6aa7cd850bb1e",[2631]="b3846c3de9f93623db09a15fcd90d3be",[2632]="332251302e7775119f650c45c06caa67",[2633]="Azeroth\\map26_33",[2634]="Azeroth\\map26_34",[2635]="Azeroth\\map26_35",[2636]="aa26c10d78bcab3b4a435e9b990adc5c",[2644]="67ba43d493e62a8fad5de319e6d4cb05",[2645]="67ba43d493e62a8fad5de319e6d4cb05",[2646]="67ba43d493e62a8fad5de319e6d4cb05",[2647]="67ba43d493e62a8fad5de319e6d4cb05",[2648]="67ba43d493e62a8fad5de319e6d4cb05",[2649]="67ba43d493e62a8fad5de319e6d4cb05",[2650]="67ba43d493e62a8fad5de319e6d4cb05",[2651]="67ba43d493e62a8fad5de319e6d4cb05",[2652]="4bf242c46045b260a8b36b43750f2956",[2653]="643a18afaf893d3ff6d7b73cff7d669d",[2654]="5793450effaf809b96c20dca57d76b93",[2655]="0e72012f25a6c52b4232ef00d654e2a9",[2656]="8a4ec4e7a31cde2eddda13f4c155272f",[2657]="fc5ab85e31e8558f11af071c64608ce8",[2658]="0e2818a815ca49041652f2ddb25c9010",[2659]="09d34299396fe043b1c60e04c0761a2a",[2722]="67ba43d493e62a8fad5de319e6d4cb05",[2723]="67ba43d493e62a8fad5de319e6d4cb05",[2724]="67ba43d493e62a8fad5de319e6d4cb05",[2725]="67ba43d493e62a8fad5de319e6d4cb05",[2726]="375ff4bdaec23e2aad1f53f0527c179e",[2727]="4d621b9c1238f4672cbadc627b984918",[2728]="db85f71b9b8ab1efb92a84a97a414993",[2729]="fa9d0716101187d80a85cbe54b7aaab4",[2730]="fcc5807e032a670ff0056eaefd310f47",[2731]="d400f93ea9bc1ee2065c16528f0ddea9",[2732]="31ddbf465834717fd178ac3c84b2e948",[2733]="Azeroth\\map27_33",[2734]="Azeroth\\map27_34",[2735]="Azeroth\\map27_35",[2736]="Azeroth\\map27_36",[2737]="67ba43d493e62a8fad5de319e6d4cb05",[2738]="67ba43d493e62a8fad5de319e6d4cb05",[2739]="67ba43d493e62a8fad5de319e6d4cb05",[2740]="67ba43d493e62a8fad5de319e6d4cb05",[2741]="67ba43d493e62a8fad5de319e6d4cb05",[2742]="67ba43d493e62a8fad5de319e6d4cb05",[2743]="67ba43d493e62a8fad5de319e6d4cb05",[2744]="67ba43d493e62a8fad5de319e6d4cb05",[2745]="67ba43d493e62a8fad5de319e6d4cb05",[2746]="67ba43d493e62a8fad5de319e6d4cb05",[2747]="67ba43d493e62a8fad5de319e6d4cb05",[2748]="67ba43d493e62a8fad5de319e6d4cb05",[2749]="7917a3d50ba149760f5ae7202d57cb0c",[2750]="f8110b0dcd0de38cf6300397e556b9f4",[2751]="915eff8c49e69287194b3aff2bbdfec2",[2752]="2a541add68c2bc5a878612d9fbbf27f0",[2753]="b61feed1b396167aab2b266dfd81c8d2",[2754]="827fcb71881537bc187e8333bae3d6fe",[2755]="88fac27848765bb4dc77610b1aefcaed",[2756]="2b3077d362ba7535e9ffd12428ada688",[2757]="e943055728d993b6aab043ce5d136023",[2758]="581c07c291d0fdd0e546f39cd0fe03d0",[2759]="745ee31354b5d7235133660919692822",[2760]="67ba43d493e62a8fad5de319e6d4cb05",[2761]="67ba43d493e62a8fad5de319e6d4cb05",[2822]="67ba43d493e62a8fad5de319e6d4cb05",[2823]="67ba43d493e62a8fad5de319e6d4cb05",[2824]="67ba43d493e62a8fad5de319e6d4cb05",[2825]="dcf37da32a08a3bee42fb0b052e48e27",[2826]="c24e85a5cce7ca14a9fe558d3344b30d",[2827]="75f08bcf4182f54f476373690bdea69a",[2828]="f8ec878bfe463a816912290cfb87d8b7",[2829]="1f2701b9b54fc499865cf4355da9b998",[2830]="53907d976dca3170bd3e47b34224512f",[2831]="a581818cc903bc7ef8b317af72b86d95",[2832]="0af1a02b3939f28bf6f8e7b8b5dc0335",[2833]="Azeroth\\map28_33",[2834]="Azeroth\\map28_34",[2835]="Azeroth\\map28_35",[2836]="Azeroth\\map28_36",[2837]="67ba43d493e62a8fad5de319e6d4cb05",[2838]="67ba43d493e62a8fad5de319e6d4cb05",[2839]="67ba43d493e62a8fad5de319e6d4cb05",[2840]="dc6ee0615915f30ef6f61347c23de11b",[2841]="20617e20390b6b305be6e6820da7c3e3",[2842]="21693cb3ba22057804e877941c909a95",[2843]="67ba43d493e62a8fad5de319e6d4cb05",[2844]="67ba43d493e62a8fad5de319e6d4cb05",[2845]="3d03dec65ee927adf5cc0b14e9dac522",[2846]="57a5d9bf20b62c44d6e0038dc4b3aa20",[2847]="de39a119fbe57ff5ed381577c34a7312",[2848]="c9371a8d5d021aa58e08d39acf9aad5b",[2849]="1b03ac965fae723f8c50d092a21c9f24",[2850]="b72a3df0f9c67525a6bdccb56bc41b61",[2851]="97eeda3fe10a407d579994d05eb22b8c",[2852]="f61ff46defaba8cad00df7dd2b0a8b4b",[2853]="0693a959ce97c43aadc781d82aca4131",[2854]="1911b23beb1a8918d317037f510b6d8c",[2855]="891ea15d788bc2001cbd271c676bd19d",[2856]="9bff6b84fa70b870be97be6c5deacf3d",[2857]="27b7f8f8896be4bf8f8c7991a4860a03",[2858]="b8c7f439ad7248b9d9978742660f7cc6",[2859]="d0d9e902e1c30602b267332fcaa0ede8",[2860]="67ba43d493e62a8fad5de319e6d4cb05",[2861]="67ba43d493e62a8fad5de319e6d4cb05",[2922]="67ba43d493e62a8fad5de319e6d4cb05",[2923]="67ba43d493e62a8fad5de319e6d4cb05",[2924]="67ba43d493e62a8fad5de319e6d4cb05",[2925]="bc7b1b18dfe4e4afc8fd96520f748a47",[2926]="34e5c31cc0483cc4dae9c59952eaccfc",[2927]="02f3f59e750b9204f3236dd95866b314",[2928]="160c86178564e5455a5bd2edc37aa4dc",[2929]="c227cf071e51a5379d8f15dfc5a7ab21",[2930]="e5641532b08c2ffbbf7c436811de05a1",[2931]="303d8fede1f036353bfe99b85419eb21",[2932]="fd46ada60ae7a7031e666dfba01ada35",[2933]="Azeroth\\map29_33",[2934]="Azeroth\\map29_34",[2935]="Azeroth\\map29_35",[2936]="Azeroth\\map29_36",[2937]="Azeroth\\map29_37",[2938]="cef5e16e8274f97616358b580334957d",[2939]="f141cf43b061c28f31318775a1154e0d",[2940]="9a03d797aeb2360d3e5a8b9e1c52ed01",[2941]="2b1d0b76fff91a836c4149b3295515a9",[2942]="e522abc92a6720777586a655fb74ee0a",[2943]="11d1e5aade1f421fbe4ed8b834330ca7",[2944]="1d81b55b064efa2089a810bc1e2ff71e",[2945]="d0893d7ca47582a3546a825c9e33faa9",[2946]="1e9697f6f03768c4458d99b354b632ba",[2947]="d064bb8279d1f3a7075b1be7b2cc79f2",[2948]="52ffd925891d628141eef390c2a5ccca",[2949]="e3150ecb569982599c9dba88f742900c",[2950]="d8c7af4b766878912f6aedb8eb43de5d",[2951]="f5b3d5f6c7923f464f32800065c8baa4",[2952]="7e7ca91838d9c4a1a33c1c0e4106c1a0",[2953]="1f677525c8195ec4f1bcaf067e7742f1",[2954]="bd3965005e56b117c8aa5d31d2f1de57",[2955]="23daced207ffa536a29d411bf9a3d841",[2956]="7aa8a6bb142241be5da5184fb1467617",[2957]="408e566bccb7011bd9c474b017ce37a9",[2958]="dca4d52ac17be9c2b302fb27d3f802df",[2959]="beb64452f9b03ed5ee976004470ca447",[2960]="07cf7f472e3f52265c7d76da1a18e54b",[2961]="67ba43d493e62a8fad5de319e6d4cb05",[3022]="67ba43d493e62a8fad5de319e6d4cb05",[3023]="67ba43d493e62a8fad5de319e6d4cb05",[3024]="b82a73915bb4a16cfbdd480d756c4ee8",[3025]="68c4d03e33206b361e803a3e7913fd37",[3026]="a88d2e4ef8102ade24789f327e2ef4a5",[3027]="c2540ee57d82e0c3f4292ba305831fed",[3028]="81334e4739b29c96832d3e55f41d6036",[3029]="b5e8b9fad0d99f7e55951d72de7b756f",[3030]="0fc031f4027a9efdeec1ae64be9d4ab4",[3031]="d694d7152d536eeaae4e65446184b2b4",[3032]="a1d1158a6ad69a7a24647918a1a49b0a",[3033]="d71f761983823a21c8e0d803abffb5ca",[3034]="Azeroth\\map30_34",[3035]="Azeroth\\map30_35",[3036]="Azeroth\\map30_36",[3037]="67ba43d493e62a8fad5de319e6d4cb05",[3038]="67ba43d493e62a8fad5de319e6d4cb05",[3039]="04fffd26817d50c00830f0164bfe1917",[3040]="faee19ffc1d155140861652a02168ded",[3041]="2a55c988fc0cdb9dff098428cfcab780",[3042]="2d3c74fa82ddabbe80663723b2bfa7ca",[3043]="7776a9052ca1992dd96b7afbc7a6c4a1",[3044]="387a61dbf19e297274f2f5303450a97f",[3045]="d15a33dcc85eb5a6a14ac3a71596ccff",[3046]="00af0fc34ba995fab25d221ef8880393",[3047]="9b3e042703c5a5f3cb1114a32248203e",[3048]="4fdc53bd15340825aeb22d5d4ce477ee",[3049]="0756d543835b641b7122ad5d852071e0",[3050]="8be42c8ca1d2ccfd18e88c49b9aae8ea",[3051]="5727150a82662694350fb44aa15a40ee",[3052]="0e3587df20a74e21b48478c3a34a59ef",[3053]="53df54d68eeeb8f23c9c6ef5cc46be09",[3054]="6c95e5eb7c6e66b00f0f6ad30f504f71",[3055]="5db246ae3cdff3429f2be60986ba59ef",[3056]="a9ecac5a04b0aec5473e0552237e59a3",[3057]="ba7273124b62803e57011dd57e20f6bc",[3058]="4bf4edfc0657c14e88b9f881750e1c52",[3059]="a31caf02a97fe2db4bf60cd71ac5f157",[3060]="1d522ab802179d97b6a3a2bd45ead9a4",[3061]="67ba43d493e62a8fad5de319e6d4cb05",[3122]="67ba43d493e62a8fad5de319e6d4cb05",[3123]="67ba43d493e62a8fad5de319e6d4cb05",[3124]="a7bf21f6a397698734ebac0088fea8d6",[3125]="3604d086eaf7788dc4a94cbcfaa24972",[3126]="60c983e786087b2265031b9fa8fa42c7",[3127]="ae2cd004c949bd75fcd7d0f0350c12f9",[3128]="4e1d93e88cc3f90b0562a47efd2194a8",[3129]="f668b8d6bb01d26d5a04870aac161778",[3130]="6bb13f72b5f71d6c1e7b5c9817819031",[3131]="f7a3bdcacfa0acb8e4a526dfc1afdaaa",[3132]="a4bb42226fce2f0e8eb247caa86479b5",[3133]="78fbe22d9ce91bf745db285826b9298d",[3134]="f6ba9e4c8295b4ae83512000e6a799f8",[3135]="fb922eb932939aed2bdf3a44d50a9f10",[3136]="67ba43d493e62a8fad5de319e6d4cb05",[3137]="e9132d1d3ed5b400cb078b13387def35",[3138]="2603d2ed06c1666071bf984e9a51a817",[3139]="ea57713ee4b2f77f2b1d1b5c6d24a2ee",[3140]="f33ccb794b050c27ca8c67cbcda2e296",[3141]="175978aab113f68cff56205677e2d0c2",[3142]="a283ec165ab18c69d72a067a5f818aa6",[3143]="f7c4d2f94af49c14b16fbab23bf2ba93",[3144]="ee0c5e9410294f3c209c9d8efed83ed3",[3145]="6aa20ea8f6772057d38f19fc3445bf97",[3146]="949b890a61de9c4d8dde503c500ff56e",[3147]="067ac84c8f546c2be06e5ae7578a2fca",[3148]="d92b49e1ee69866016622cb3ab3407f0",[3149]="8b89a1a8708c4fd57cf24ba14c79c755",[3150]="364eb6acd4af5327099bebd360b5671e",[3151]="05dbcad95cbee665d47145bad381b72a",[3152]="89d7c5e7175d3c784ccef4c9f3245dd1",[3153]="9112856e7e97f9827200c8b56616b52c",[3154]="e456f004ed33de4311d5fb56729fecc5",[3155]="1dfbe3b5a5c5c9f6b150d2f760a55431",[3156]="7ed851fd234074f2f7b651df4957514d",[3157]="6890ed60522e0e5ca5be8a99e0151606",[3158]="85ccab6c7b060891496b1738856ed94d",[3159]="f28cbc91710d22704390d993b081e3ac",[3160]="e3f87d53df095c29dc1d7bccbb57d62f",[3161]="67ba43d493e62a8fad5de319e6d4cb05",[3222]="67ba43d493e62a8fad5de319e6d4cb05",[3223]="67ba43d493e62a8fad5de319e6d4cb05",[3224]="c315a664904ec3784e7104bfa429a72b",[3225]="9ced3a185cd45fe1285fd8583625e5e1",[3226]="d457cac1a70be67e8d767049eb9d5c6e",[3227]="e8b627a46fb1701dafea7fd027839d1f",[3228]="55b1c8d6668670a6dfd76edfc60e4da7",[3229]="e36dc1da93489071d8a3167065a410d0",[3230]="44d9053282929a60884e909142ff629d",[3231]="1357b2221b80394db7cf75ea7c4b1f6c",[3232]="6cc087b26eab6f8a75d13e678903d4d5",[3233]="81b061bd2383ca88fdfb46f3fdea4e7c",[3234]="879d2c65e694d8b9dfa4a5506988dec6",[3235]="67ba43d493e62a8fad5de319e6d4cb05",[3236]="c54e1267004790538783a862e48ab372",[3237]="d0d41c38b0cfa52c020d2f8117bcd83b",[3238]="4b59bd385d38b180653a930d3f82dece",[3239]="4e4c84c162f1ebf2194e370ab2f84b81",[3240]="030640836e01608f13a9c0db19384463",[3241]="3a328d79a195704a6ec27516d04402a9",[3242]="65ead1f686e992d9653d91c6d4760ffe",[3243]="70ac573bd7d9d99f9d308c201b61c5da",[3244]="f0ec481c8a97f25928aee44ff139f3b2",[3245]="ad7fd065ce9b57b022041727f128703e",[3246]="8ddefe4d9c33b3bde6f91782f38bf6c7",[3247]="aaf2b2bf21b08f005b410225c50f9bc2",[3248]="ea283abc0bf9637c3fad5e840a65b38b",[3249]="f743d57d3463fc6df4e629423d6aa576",[3250]="4bd0bcae519326446fc80248f15204a4",[3251]="c69d18b0703f40117eb5b3772aedb8d6",[3252]="01977102194646854a915529cf32679a",[3253]="258247463f47519a6f1b8e0037cf952c",[3254]="00c0672fa3fe7471cc2e90bfefdcdaac",[3255]="c7f3d5069409f51e0827e4909fb6a3ae",[3256]="61abd761668339f20ec1f459ab8a201c",[3257]="a6c7653b9a68620ea5066cb81bfa2a3d",[3258]="b3623bedb450e5c0d332e009bb982c47",[3259]="03acc8d975ccb085c386bc7f223b7942",[3260]="9f6d3cfb66a8aa654cc2a96edfcb21c1",[3261]="67ba43d493e62a8fad5de319e6d4cb05",[3322]="67ba43d493e62a8fad5de319e6d4cb05",[3323]="67ba43d493e62a8fad5de319e6d4cb05",[3324]="4164f211735b83ff2d852231ea5377e1",[3325]="3991b940746792f4269361ce0b3756e7",[3326]="5160df551204ddf1ba9b203ee28fa91d",[3327]="396682bf1970482cc4b7b1be2d99dce5",[3328]="c03be3b842b70f71a6044aadad7f8caa",[3329]="661839bfb032cfbc40ff572871064ebc",[3330]="342f2f9f32b2c9e2a9f803bf9a672246",[3331]="f7005b92d549bd6f899e7fca325113fd",[3332]="954924c1f311d207056b1c8e36cdfe66",[3333]="1786e8bd159c991500aa44a53d446f49",[3334]="8c7aa296488e9f83c0276070124104e8",[3335]="3e8a144a5b41687b23c8d856fed81435",[3336]="57d5329a1636eb57d025946e7c498e39",[3337]="7d35d4e9d1cf33886410d17b708efbd7",[3338]="14ed5c597aade652ab045a59de457248",[3339]="566628ae32a50a37dfe97a1cf5f00c83",[3340]="e50d17d9bf70998cd241e703899dbc7e",[3341]="a5422c5d64b51e397002b34276fb3d22",[3342]="e1f49f7f797755fde57fcde1fc884102",[3343]="69924988508980bf6614748bf8524d11",[3344]="58eeb98e70e81ac3a647e13038e5b78d",[3345]="319992ff4c41e450e4fc398551dd72a6",[3346]="87d1ba3750ff9d730ad2be9e4552041e",[3347]="381fad022b7a21f96ac1e917d95910c6",[3348]="cc1e1e5c66ac3c5891633259430253a4",[3349]="6f92966dae6a4fabce321a0944281746",[3350]="8f6266e42fb8ae302d2a1d5650ff45af",[3351]="5eb877c2be769bd35f29e4b5d50d6541",[3352]="c141a3e6056cc2264fbeac47d0138270",[3353]="336fdb7f0c893496d301b5fb20933d4c",[3354]="07d3d19f9bf3b0d934677a2b13491989",[3355]="40072de4893d8f79685021fdcd7ff2d8",[3356]="2ec09e010b41b845a9e96e07ea1b3bb2",[3357]="bdba45d2d4bc0d1725f547e18a83d551",[3358]="90bcbeba892fb57095775a2361d8cbef",[3359]="61c338bd7fe65d191f3b13eed12b9cc9",[3360]="7dd56cabfb38dc2b7d298bd1fff73a41",[3361]="67ba43d493e62a8fad5de319e6d4cb05",[3422]="Azeroth\\map34_22",[3423]="Azeroth\\map34_23",[3424]="Azeroth\\map34_24",[3425]="Azeroth\\map34_25",[3426]="85f8a4e7bf5981ae815fb052a84eebaa",[3427]="800fe9d756b4626c21b58078c057302e",[3428]="1b31e2ac11852c27a818c35d13397d88",[3429]="ecad735662844156e572b6c736b2541a",[3430]="caa2a0b516aa308a9e09047e6d735291",[3431]="c620352a97026b7fddbed244815bb5a4",[3432]="fb0b9a24a86658b17830964e3bfa9c21",[3433]="f34992098de302a4b83251a26ca8c411",[3434]="024935cfe4293d3d620ea78b8aa54fd9",[3435]="b2dfade430f87325743055317bc0ba76",[3436]="2c90da244312444fc16aef99ff8ab239",[3437]="9f7b3a65c52007b829f7a65c6586ef04",[3438]="331fa2f2e2a7f08691f844765225a063",[3439]="0dac9d5270cbbcda981355c7f9086331",[3440]="e63a713f9f1fea017e2f30859d960612",[3441]="f3356f6bce8bacd75c4ab21b1440335e",[3442]="1e085b8267b842ec88c6b74e81e6d045",[3443]="70f8d2b9f154c3dee700ad9f5068b195",[3444]="1ee5b3f0b68d0abc823e32d7a8d88acb",[3445]="a15492159a0bde91ccd01474f47ad246",[3446]="783b4ee8979b7823ec15792a22b45e2d",[3447]="9fffe354b5c004fc22684c76ce478e05",[3448]="f55e5ed5f5379ca2ca32c4769e1012a1",[3449]="ac59ccbbca5b9c6e144f9a172f16886e",[3450]="989913ce8d0f5f5a6a8ca902354149d7",[3451]="d38e8a8f13746dc6e1f2e082a9a27972",[3452]="409f38ed231a41d4845cc5fa10aaaefe",[3453]="619413c8549117aa1606cdeadf0c50a6",[3454]="a6a286ed1ceb370b27b2f7840f346c15",[3455]="c82daea8647f08f159bb391b03347437",[3456]="fba8da0b41aef1fdd5de9cdbdd320d02",[3457]="4c89eb0a4c16dedee11d0e26d8d59d6e",[3458]="979e9ce486bc462971a1947f3ec16490",[3459]="11edb63759f1654e7541ad6bf94f91ea",[3460]="0a4b5797ebb9c5e45b87973c79966f10",[3461]="67ba43d493e62a8fad5de319e6d4cb05",[3520]="67ba43d493e62a8fad5de319e6d4cb05",[3521]="67ba43d493e62a8fad5de319e6d4cb05",[3522]="67ba43d493e62a8fad5de319e6d4cb05",[3523]="Azeroth\\map35_23",[3524]="Azeroth\\map35_24",[3525]="Azeroth\\map35_25",[3526]="Azeroth\\map35_26",[3527]="cd63b2cccce76aa64176b6ae18138cb9",[3528]="78ebba45ead104322422c5643ed0e42e",[3529]="5289a87cd82882e097c7d5633af6d08f",[3530]="ea396e1cf07f7cedf6f9b4e1aa45a612",[3531]="210caefb3a65e0453ae1c14baa8fc381",[3532]="4c12e4ad53894611502264ae303d6d8f",[3533]="f5726c49e003b941886c4a9d6e6c515e",[3534]="b71e051063fcc9b95edb4e937d6f6aa9",[3535]="de1344b789115e79a3ed673038c0bb4d",[3536]="5d74549fbaef091b8456cc515ff4ee47",[3537]="921c1fb52ccc97ffe9c8103290b1de00",[3538]="ec214038c7d9a53eeadb489e35bcc6ef",[3539]="c7ce6a1c168ce3a83111b49b9b6a106e",[3540]="27c2c304854fbdb002e20606f44ba689",[3541]="fff4e7ea9f585853a9ee3aec874b35b0",[3542]="e2331d8c2d979458c42218fac07a888e",[3543]="eee648201d5027a2033f78d7a531f6b9",[3544]="8c99da3d8495a041a9b479d354c8e5db",[3545]="27b4bcf3d73e74bdec0f00336977f607",[3546]="d95edc39c5b5551398783f37fa1a323b",[3547]="9a5a8f7bdd758c1d8bff3f495829386f",[3548]="341124b74fdbfd2957461c4bb1d3daf5",[3549]="cc1cc023179db36a3f25d67270a2edba",[3550]="3a5867eaef15aaa1ed8344ecafb39d68",[3551]="8b75b5477bca03361f5c4f3f35407ef6",[3552]="9f1b101c8b49ac5538114459efb39af9",[3553]="768b970b7658beaaf6d326dbf790c80a",[3554]="65a919da04ad9c2284aed1f9eb642763",[3555]="2d212deb08329a300464575930273102",[3556]="94d267a52f02ea739a1ed5846d033967",[3557]="0a01ef529eea43992197d22fa6bbc0d4",[3558]="6f905a8836931fa58aecbe460488cdd7",[3559]="7ec65c77541b5d6974ee1c0e3776be96",[3620]="67ba43d493e62a8fad5de319e6d4cb05",[3621]="67ba43d493e62a8fad5de319e6d4cb05",[3622]="Azeroth\\map36_22",[3623]="Azeroth\\map36_23",[3624]="Azeroth\\map36_24",[3625]="Azeroth\\map36_25",[3626]="Azeroth\\map36_26",[3627]="Azeroth\\map36_27",[3628]="c142aff8e2c7b26b90fa3b4b765f2c33",[3629]="81294afc613412beda415c8ef8fef523",[3630]="77ade5b3731dc13a869e5ce94297c422",[3631]="33a8a1ed74f28f415675ab7830203911",[3632]="973b9481895581debba56946eb6c1db8",[3633]="a44f6f3ade60234a61fa037a905daa88",[3634]="7f7be2b7a2c0e685f23f24169bac02be",[3635]="130b6519c832c05c892732ae70004229",[3636]="279bb1969232057e4ccb240d609fd71b",[3637]="b59dfc3e193d9127ae01d18e69a3f726",[3638]="439e83bc0c0519cf4a2c659d73cf8b42",[3639]="b5e93703a0de6f547b2253549d232478",[3640]="e35bfe6177ef815be8509dedcefa7a6f",[3641]="79597a3b8a0c100a3698707caebe6ed9",[3642]="7b2633216a234e071fe8621cc9bace7e",[3643]="8ece05c2cb7d9ac16699d8e6d340103e",[3644]="55e33b4a5580e08a0b0a100c670a2675",[3645]="33c131ad8c8ba326c1e8b38c3f23e283",[3646]="75c3df23723a0480114f80d4a8b84cfc",[3647]="47775d71af680bb69af0d2081e51c36a",[3648]="405544c7989d1f1f7975e6da533a6ae9",[3649]="e4bb2b32906d9f20f739e83edd63f7d2",[3650]="5df61389133361df2cb5eb43309c268e",[3651]="cee37228c45b1b9e697b0ce075cab693",[3652]="3dd6279e35908a3923bd87bcd2219843",[3653]="b5495897bd4833751deb27a2ed22c894",[3654]="cf584aea412c82e4b42cc5424a1d1da5",[3655]="f9ef3e6c36c89e0d1c0e2e035322420e",[3656]="0a522b3a2f9632029c413023cda67038",[3657]="67ba43d493e62a8fad5de319e6d4cb05",[3658]="7ec65c77541b5d6974ee1c0e3776be96",[3659]="7ec65c77541b5d6974ee1c0e3776be96",[3720]="67ba43d493e62a8fad5de319e6d4cb05",[3721]="67ba43d493e62a8fad5de319e6d4cb05",[3722]="Azeroth\\map37_22",[3723]="Azeroth\\map37_23",[3724]="Azeroth\\map37_24",[3725]="Azeroth\\map37_25",[3726]="Azeroth\\map37_26",[3727]="Azeroth\\map37_27",[3728]="b2de1d4ae1786b529215c197e8f9eaf1",[3729]="97430e8b9cd799d9b3aaa8dfe5675892",[3730]="0dfad49ec79cc96f41a2f4b52370e4aa",[3731]="4c5b611fffd9d8d5f4bf8a58c7d33a0c",[3732]="6b307396452875cd5bad6fdf3dd4c3c7",[3733]="8b8f977e86f739f87b46387264308f53",[3734]="92354b500a4f8b45316e227e3542e604",[3735]="56db8535336e971ddf4687e047b657b8",[3736]="8ddb1312857de506591dcbf117a8ce88",[3737]="ad6e372dc1019c51972d05a28f03cfe6",[3738]="3bd00d3be34380dc4ab10dca38c885f4",[3739]="0964d1257b20adc91880170fc7436975",[3740]="f911322131b86828ad20c129759a0c2f",[3741]="15d260d223bc7ea19ac3f8dca2b237e5",[3742]="cc6ff21b73ad0f4be95877eb37337915",[3743]="727dfa6a3db249a24f8cf9c4f472903d",[3744]="c665ce51205225b13cbcb2863bf38208",[3745]="6a21496e69d370ee232b08323d307297",[3746]="a4171be59152bc613537e24739e60372",[3747]="d426ebb9d6a4ae414414f9ef8df45c0f",[3748]="e4f124e3023b6226d6ff3d3f2361bc6a",[3749]="d4732c9063c4d57281d1fe231f99ac5c",[3750]="7a1a172866fa5d51e420e3f6d381b5b3",[3751]="eef3fdcaf1843fc9308c51d5cd849b12",[3752]="0cc6e3c4cde6c39dba79222e98a2b33c",[3753]="58bae9872bf403ffe17b271360eb3fda",[3754]="8097163e3663802e3fbc03dec9d9ff90",[3755]="8b271eeb6cf449f770a053c495c7dcbe",[3756]="645bc5e1ce553536a2067d7059ee74c0",[3757]="7ec65c77541b5d6974ee1c0e3776be96",[3758]="7ec65c77541b5d6974ee1c0e3776be96",[3759]="7ec65c77541b5d6974ee1c0e3776be96",[3820]="67ba43d493e62a8fad5de319e6d4cb05",[3821]="67ba43d493e62a8fad5de319e6d4cb05",[3822]="Azeroth\\map38_22",[3823]="Azeroth\\map38_23",[3824]="0e1232b3af047bf79a1d0c45093c7232",[3825]="52c03d8660a842fa93be589ba9685388",[3826]="0bb52648b11b0f2218cac80e419b75e3",[3827]="30fce8abe68c6fe7f3a7ed47340fac2c",[3828]="2d930aca6f95f5c176a4831e3acd70c2",[3829]="22038358e7317b1ace845b15da6c5cb3",[3830]="e6b86fefc36af816f846f980cf0a3020",[3831]="c452b1f6c1bae548fd31da1bcb5069bc",[3832]="02ba650ca9f00c64efc9503149950629",[3833]="e20a2bb7363f1e5c70259bb4fd448a98",[3834]="2ae50f84ef71fe68419da964dd1175c6",[3835]="d0658ff8c712083ef653ec4e3b2d11a2",[3836]="976c534f846c112e91b0752a30616370",[3837]="b9ed261eb31cedf645235518ce74615f",[3838]="5ae58fefb284c222e6732bfca110f0da",[3839]="33204161740d72ccc2614acce975ed77",[3840]="e96d0f13152c58d6896aab30f3261839",[3841]="ba629724c6531df4a279a9e0ebf42183",[3842]="339dae61701d07d762c6e209c06e8e60",[3843]="c7bb562a39273b82e9259c637dffc943",[3844]="e44d965996a20b31ee1b5159599ad9be",[3845]="168d7c4d777d2cf1cd0918982c690d37",[3846]="8a0238141e755094d4389139c635465c",[3847]="57da0ee2177cf2935644c7da87f2cbed",[3848]="07ceac579b53903ff871c7cf382376b5",[3849]="fdd319c01d3fd55d343ca0fc96b1ae3d",[3850]="3e1a64156e3f523f7d02ff9762ee311d",[3851]="db3dd061b7e3b3cba35e26864bf48c7d",[3852]="81a7fe116b33d97d6ea22922fefffb08",[3853]="bd5f03e5e5c12f6badeb97fd69fd9a50",[3854]="01c32bd0b10e7e2ddf2d984bd3f756f4",[3855]="4210d975503348446f370c87b36ce2ef",[3856]="67ba43d493e62a8fad5de319e6d4cb05",[3857]="7ec65c77541b5d6974ee1c0e3776be96",[3858]="7ec65c77541b5d6974ee1c0e3776be96",[3859]="7ec65c77541b5d6974ee1c0e3776be96",[3920]="67ba43d493e62a8fad5de319e6d4cb05",[3921]="67ba43d493e62a8fad5de319e6d4cb05",[3922]="67ba43d493e62a8fad5de319e6d4cb05",[3923]="ec187d6e387a23ddb8e4cfb53fb4b34c",[3924]="4b8c99e6261aaf6cdc414a4b70fb24d3",[3925]="d35be22b703831ec4547d87c813a49a6",[3926]="8ba667c92205a45e2fd45c8d31cfb37c",[3927]="25da901b4a514ddab7f9f337b5720d98",[3928]="4c7af87cf9a66b7a99303366205140cc",[3929]="25853345559a8afbf8396d443d2509ea",[3930]="4f118530542beebc4d9869a6c5eb0508",[3931]="4bcdffe810e7c79785497b804fa7174b",[3932]="b5494b6fc6ffdfd60810e01966465d66",[3933]="c70d2961340b7f17062ef9cbc2db4324",[3934]="fdf02c2d6d3d2d0120d03628edbde9ab",[3935]="2b0bd4ff646af940a90113c33c7a987a",[3936]="7697a849cb4c0a3b647fb9dce0560c16",[3937]="d73089b817f990a6da221b6d14dbcc08",[3938]="5968e218aa10ad89539a88c2264d8544",[3939]="f89e36a074d427574e681731decd0e57",[3940]="880aec1af9372a0cd3b67af7a48ee3b9",[3941]="75c9bf7a93c870152897e95cddcc1f46",[3942]="f83ae0ea4c03828843fc27b13073877e",[3943]="7adff4bfdb23404466ab93baeefc9273",[3944]="a6ee857b95e6563fb1111e3fdf369305",[3945]="928d23a144c85cceb120c74ce5f567fc",[3946]="315c6a4eefa498c93a880bcd4515c0e8",[3947]="36dbae04cba749999ff00d736f3f2601",[3948]="e9dc75eb8f89c151997e2dc7b2b937f2",[3949]="a1594068d3182de0b59562d8d618c2c3",[3950]="24180739dae18e01a4f947eb882a3d3c",[3951]="4539bbc1e3850b0c453a3ce186da0178",[3952]="9942dcbb50a6c31e08ddd2fb4cbe9684",[3953]="8becad2263ce65dba27d9c9bd4faa0b2",[3954]="8949370548378912d3ef01ca1b5a4a24",[3955]="36dce6e49b0c39fb3cff922b4812c32a",[3956]="67ba43d493e62a8fad5de319e6d4cb05",[3957]="7ec65c77541b5d6974ee1c0e3776be96",[3958]="7ec65c77541b5d6974ee1c0e3776be96",[3959]="7ec65c77541b5d6974ee1c0e3776be96",[4020]="67ba43d493e62a8fad5de319e6d4cb05",[4021]="67ba43d493e62a8fad5de319e6d4cb05",[4022]="67ba43d493e62a8fad5de319e6d4cb05",[4023]="79ce67678556180bc913e3926f4ed870",[4024]="dd0f3f220de3d9451f61fdfa6f4ecde2",[4025]="c8448722adef148d2f4b3290fdeb4916",[4026]="4afca6bc87fdc2306d83aa8fc29e7a80",[4027]="8c393a1f92fe561322318974be2782e2",[4028]="e50ad59b260b47bb305740b6a554d7df",[4029]="4e352ec05d4f8b68f58c18e5775de226",[4030]="168776ab3795eb8b545f3edd94bfdb65",[4031]="e0ef25f4eb8ac765bdfdc7850295dda8",[4032]="7a8d397d13cadd8f438ffaac4c9e589a",[4033]="6b65254061473c063e34a8923fa87486",[4034]="92d20afe4c57921fa8b8d987f6734874",[4035]="157b0fe500f07f13b002030fbecf47c6",[4036]="67ba43d493e62a8fad5de319e6d4cb05",[4037]="dea9784bdd986380361fc918ceec1abe",[4038]="78a6ba2810a90fdcc83feb704bed3367",[4039]="47cc892696ea3be72606f3b3e2da8163",[4040]="e8c96408a2ef8beaacd6e7b57fe22e4a",[4041]="81d17717175ff8d076928b44f800fe7c",[4042]="e20fb4bc7a6da29f1e54be41b1cb4ef9",[4043]="4b4dbe28b29f537fa035b3c305faf78c",[4044]="013c98dccdf6ff2ab53702c7b915848a",[4045]="cca645ce8734603ec6af8eeb25b8cb9b",[4046]="1b59c8114ac513ef0f910ccc6ae981d3",[4047]="8c3a19ef2a0a5a43b10617e3cccb5d40",[4048]="2d8c05f81561e39f759e38967d07df19",[4049]="8605967d8b492763d8828e7c668ea475",[4050]="c87f632a8147dac95da028268e290059",[4051]="76d1561b37940ff3f2df8805b061244c",[4052]="b75cfc6776fb41ba916b74c8931ad781",[4053]="ec8a77e198d49f288b618602a4b40834",[4054]="67ba43d493e62a8fad5de319e6d4cb05",[4055]="67ba43d493e62a8fad5de319e6d4cb05",[4056]="67ba43d493e62a8fad5de319e6d4cb05",[4057]="e4ee35d9b650643d4a488037afea775c",[4058]="e4ee35d9b650643d4a488037afea775c",[4059]="e4ee35d9b650643d4a488037afea775c",[4122]="67ba43d493e62a8fad5de319e6d4cb05",[4123]="04c7a652df2a5b47e08dad89303752bd",[4124]="f1dda9c4880279491066bff66ccf27cf",[4125]="cba0d455ebeae488b5572d48c033a763",[4126]="43f9cc49b5b8e2cbd295283a8b38620b",[4127]="ffceaa917ecd324a3fe07e66d1a15b97",[4128]="67a8839d01de1a64501051b0b462d6de",[4129]="a89eb75dca57b3fb036049720bbe61cf",[4130]="acf2852b548dadf05ac0f07db34cfad9",[4131]="a5e7c4dfceb3f32a394a864f77ca1b46",[4132]="a83d2e6a891bbda0d9d5df7389970dd9",[4133]="be0bac81c5f86c9ca2c332592285bb67",[4134]="67ba43d493e62a8fad5de319e6d4cb05",[4135]="67ba43d493e62a8fad5de319e6d4cb05",[4136]="67ba43d493e62a8fad5de319e6d4cb05",[4137]="67ba43d493e62a8fad5de319e6d4cb05",[4138]="67ba43d493e62a8fad5de319e6d4cb05",[4139]="0c79133c43c98b0406cd4ef46c4dca2c",[4140]="79abb9c0527f576fe7d1fbd6e373c0a8",[4141]="bc4735d2a8b00ec277edff40926d0dc9",[4142]="1d52b5a65144f98beea86f8569ed98cb",[4143]="2e684550744e83f9c221223c0718bc99",[4144]="0b3a0842bbe76d4dcdd6230f0e5b7910",[4145]="99f6bede38147d6a310efbfe4a9e56b9",[4146]="85364f8abc9abe4799fe62e95377e169",[4147]="610cde8dab05704fa72bc80d92c9052b",[4148]="4528945f913a0942dabbcb036a96b840",[4149]="8310fd0b62e9664a85b06c0482e1a156",[4150]="d4a79605818744d830dece891e9399b9",[4151]="76febdad0f30d54826b4d51b9342b909",[4152]="5b54218737f7f156d670aa157588d6c3",[4153]="67ba43d493e62a8fad5de319e6d4cb05",[4222]="67ba43d493e62a8fad5de319e6d4cb05",[4223]="67ba43d493e62a8fad5de319e6d4cb05",[4224]="0b21cb9d5d1acb6d6006f8377ac0fa9c",[4225]="1c25329ae99200959689bb691e3a9ca9",[4226]="5b98deac4f240cc92227d091f3a5be2a",[4227]="05be693331fe99de95cfa76f2f6afe18",[4228]="72e8e03bcd939f8fcc06dc55ed1641cf",[4229]="1e1e880578fe844c57098204ea58ce16",[4230]="569996ec2754ccaf4d8c4b1bac6fee48",[4231]="67ba43d493e62a8fad5de319e6d4cb05",[4232]="67ba43d493e62a8fad5de319e6d4cb05",[4233]="67ba43d493e62a8fad5de319e6d4cb05",[4234]="67ba43d493e62a8fad5de319e6d4cb05",[4235]="67ba43d493e62a8fad5de319e6d4cb05",[4236]="67ba43d493e62a8fad5de319e6d4cb05",[4237]="67ba43d493e62a8fad5de319e6d4cb05",[4238]="67ba43d493e62a8fad5de319e6d4cb05",[4239]="67ba43d493e62a8fad5de319e6d4cb05",[4240]="2ccfd33e7b75e84b3e4ff0512e06b954",[4241]="d8ab00015efc136dd364241beedae38c",[4242]="67ba43d493e62a8fad5de319e6d4cb05",[4243]="67ba43d493e62a8fad5de319e6d4cb05",[4244]="67ba43d493e62a8fad5de319e6d4cb05",[4245]="67ba43d493e62a8fad5de319e6d4cb05",[4246]="67ba43d493e62a8fad5de319e6d4cb05",[4247]="67ba43d493e62a8fad5de319e6d4cb05",[4248]="67ba43d493e62a8fad5de319e6d4cb05",[4249]="67ba43d493e62a8fad5de319e6d4cb05",[4250]="67ba43d493e62a8fad5de319e6d4cb05",[4251]="67ba43d493e62a8fad5de319e6d4cb05",[4252]="67ba43d493e62a8fad5de319e6d4cb05",[4322]="67ba43d493e62a8fad5de319e6d4cb05",[4323]="67ba43d493e62a8fad5de319e6d4cb05",[4324]="67ba43d493e62a8fad5de319e6d4cb05",[4325]="6da6c0548b1d5f4d1cc8e6a529caf337",[4326]="3eefb09e48f2fe57028c9726813811f2",[4327]="ed41af1b7f4884706618da3130390e53",[4328]="04a81f0550a2d826821eea6f5c431aae",[4329]="2fe7ac4ada564d6b5420f714e9d23b03",[4330]="3cc60e7b989cb9ee721f9977231f3e99",[4331]="67ba43d493e62a8fad5de319e6d4cb05",[4332]="67ba43d493e62a8fad5de319e6d4cb05",[4333]="67ba43d493e62a8fad5de319e6d4cb05",[4334]="67ba43d493e62a8fad5de319e6d4cb05",[4425]="a01e86c237b459617bfa6223d7bf798d",[4426]="a5b9d0d4b54940c94634a58cf7686a8c",[4427]="67ba43d493e62a8fad5de319e6d4cb05",[4428]="67ba43d493e62a8fad5de319e6d4cb05",[4429]="67ba43d493e62a8fad5de319e6d4cb05",[4430]="67ba43d493e62a8fad5de319e6d4cb05"}
Map.KMB = {[0000]="56c593320dbef597058dc1a8082b754b",[0001]="e3b9a673a3cab38354a5e900e848fb67",[0002]="0a2b65a92e1cd93460227e062f6f2044",[0100]="f0c6e4c917ebb2f027b911d9daa6fd09",[0101]="b80f21462437000d62e313d1ad42ac53",[0102]="273ef8069fe985ade945945ebc6263bf",[0200]="c9127126b4708f1f265067b2447e8194",[0201]="2311865c234a9cc2f52e43330d909ca4",[0202]="e8894bdd894817d76cdfd6011757002a",[2337]="67ba43d493e62a8fad5de319e6d4cb05",[2338]="67ba43d493e62a8fad5de319e6d4cb05",[2339]="67ba43d493e62a8fad5de319e6d4cb05",[2340]="67ba43d493e62a8fad5de319e6d4cb05",[2341]="80bec92cc8550e114a99fa6cce3ba1f5",[2342]="fd5c57d60d6d53f038f291c94415d881",[2343]="c26e959d9a5d1f554f7ebc2fea176d90",[2344]="db4455404cadb7f94086bcac69c2873b",[2345]="7ecc501acfb2cda07cf65b6afe2f1c48",[2433]="67ba43d493e62a8fad5de319e6d4cb05",[2434]="67ba43d493e62a8fad5de319e6d4cb05",[2435]="67ba43d493e62a8fad5de319e6d4cb05",[2436]="67ba43d493e62a8fad5de319e6d4cb05",[2437]="67ba43d493e62a8fad5de319e6d4cb05",[2438]="67ba43d493e62a8fad5de319e6d4cb05",[2439]="b958d909a26e52a99e70a49777f6ebea",[2440]="39c28d49deb0593159700a29ef4f173b",[2441]="1b2628205f6a8642c980cc961261c3ac",[2442]="c6a543c7596e4554ad2046682fa17004",[2443]="94395c40f7909ca95218bed40a6fd090",[2444]="33110c5470a03b9b60e25bbd04862740",[2445]="aaec51f6edf5a1aca42e5eee6016933e",[2509]="67ba43d493e62a8fad5de319e6d4cb05",[2510]="67ba43d493e62a8fad5de319e6d4cb05",[2511]="e4be74539b8e1fceb2430d070e6157c6",[2512]="9650bd27aa835163103dd2ad4fa1dfae",[2513]="7d3c7671edf9a3c93dc5e64eddb889a8",[2514]="233f5dd7217c9587c0bfcf9506de4be3",[2515]="f3d8aff0db3283a677ee3e292b27660b",[2516]="67ba43d493e62a8fad5de319e6d4cb05",[2517]="67ba43d493e62a8fad5de319e6d4cb05",[2518]="67ba43d493e62a8fad5de319e6d4cb05",[2519]="67ba43d493e62a8fad5de319e6d4cb05",[2520]="67ba43d493e62a8fad5de319e6d4cb05",[2521]="67ba43d493e62a8fad5de319e6d4cb05",[2522]="67ba43d493e62a8fad5de319e6d4cb05",[2523]="67ba43d493e62a8fad5de319e6d4cb05",[2524]="67ba43d493e62a8fad5de319e6d4cb05",[2525]="67ba43d493e62a8fad5de319e6d4cb05",[2526]="67ba43d493e62a8fad5de319e6d4cb05",[2527]="67ba43d493e62a8fad5de319e6d4cb05",[2528]="67ba43d493e62a8fad5de319e6d4cb05",[2529]="67ba43d493e62a8fad5de319e6d4cb05",[2530]="67ba43d493e62a8fad5de319e6d4cb05",[2531]="67ba43d493e62a8fad5de319e6d4cb05",[2532]="67ba43d493e62a8fad5de319e6d4cb05",[2533]="a7f18cd45bd51938aea8d0209d2b921f",[2534]="e53c538ca8f8db95591302ee488db6d1",[2535]="532833c86f4714fd76e161c49f7a58d2",[2536]="78c90513d27ca08bc8514a0d9123d5ce",[2537]="a06bc6edf1e0fea875a69ba4e40b6d0c",[2538]="14e7f4dce3e20989bd721b4bcc342e59",[2539]="77bb70c4c88122159ea8dfa6b5d68085",[2540]="ea9a46d2e4066ca5f22ec9efa11c5c6c",[2541]="2bf0c0bb39bdc3199b644e5a6e4ea750",[2542]="0485e7e37de0bd7163667d40adcf905e",[2543]="7645ff4417be925df51fe4e32ec917fc",[2544]="5bd955cfd6d75a5da55cd161d4c7c728",[2545]="ce9b968a1dae6e2c56b0591971890459",[2546]="302e63eff98a8f4166f7431d9ef9af70",[2547]="67ba43d493e62a8fad5de319e6d4cb05",[2548]="67ba43d493e62a8fad5de319e6d4cb05",[2549]="67ba43d493e62a8fad5de319e6d4cb05",[2550]="67ba43d493e62a8fad5de319e6d4cb05",[2551]="67ba43d493e62a8fad5de319e6d4cb05",[2552]="67ba43d493e62a8fad5de319e6d4cb05",[2553]="67ba43d493e62a8fad5de319e6d4cb05",[2554]="67ba43d493e62a8fad5de319e6d4cb05",[2555]="67ba43d493e62a8fad5de319e6d4cb05",[2609]="5f366cb4b793d4951b9b11b640f54e08",[2610]="b8b7a930bd30336bc86a802b1c2f3795",[2611]="142ea3c120b93819b937e522e0e50679",[2612]="1071c46b14b06bd016fe8388e4e2c37f",[2613]="71378ad23fb237954a3a9fef30a6266d",[2614]="ddc97d9e6f63496d6ef94bf1e9ded3ae",[2615]="f068b1fbae43cf4458715b00a9448bf9",[2616]="2b90aa0699fe5c53844e9252b09314cb",[2617]="67ba43d493e62a8fad5de319e6d4cb05",[2618]="67ba43d493e62a8fad5de319e6d4cb05",[2619]="67ba43d493e62a8fad5de319e6d4cb05",[2620]="67ba43d493e62a8fad5de319e6d4cb05",[2621]="67ba43d493e62a8fad5de319e6d4cb05",[2622]="67ba43d493e62a8fad5de319e6d4cb05",[2623]="67ba43d493e62a8fad5de319e6d4cb05",[2624]="27f61c2518cb806c1fd403e6ba555bb5",[2625]="723e6dac87a6e3bf43b05630ebfc02d8",[2626]="1d19f18e7af80f59242890aa2e60385c",[2627]="347fe9fc2837478bcf623f8b0d3fd96a",[2628]="aa6020c8dbf312d38c9bf159b88ebe9d",[2629]="67ba43d493e62a8fad5de319e6d4cb05",[2630]="67ba43d493e62a8fad5de319e6d4cb05",[2631]="930e3c3e28a02df64ebe8df6b8206e8d",[2632]="d552c88f41756cf7d8594f1a98f1436e",[2633]="7a0cef8eb3c97ba74dd58a5923671139",[2634]="0c604f64734d57a75874f3445ea18439",[2635]="d4cd6e3d6726a4230d8daf6cf8878533",[2636]="7b361f2627021b3f8d3af0322c84ab7e",[2637]="908aa894b63a5bcb551dc005702d38e6",[2638]="a2d49c6fdd09113529de904087d4056a",[2639]="1a8be63886dbbea18236b3dfd70225e5",[2640]="70e1185212be35aa8a8644a56c83a440",[2641]="2633425c97927e36467e3ad74501b275",[2642]="88ca1514b64f49759d8035c869b720c2",[2643]="cb3f083faf50b72b177e89def2c0669b",[2644]="92a6d459d6eb5453ac88b0d0e9778043",[2645]="e1ba6d5131d54f737a7d9218e7d4958f",[2646]="3eb65cb2dcc3e8e9b3c967edb25eac36",[2647]="caef4150fff703776fabfa9a3917d898",[2648]="28d144235f2a8c3e79bdc4014df149d2",[2649]="02cfbeced915d96fad7466d06d7e9a55",[2650]="48570dc672540b399782d7ce9dafbe97",[2651]="47dc52281856638cd0e40debe5828659",[2652]="67ba43d493e62a8fad5de319e6d4cb05",[2653]="67ba43d493e62a8fad5de319e6d4cb05",[2654]="67ba43d493e62a8fad5de319e6d4cb05",[2655]="67ba43d493e62a8fad5de319e6d4cb05",[2709]="3a04423f16ff440dbf5fc5075c4e42f0",[2710]="60193b290b3ad84cd3ce3c9bea732fb6",[2711]="bab9ba9837a9da5a23269f08bb10c6c0",[2712]="0d90484edde094d9902320fab1a9ba5a",[2713]="abedd5717445758507f0f8f8e53bea41",[2714]="94ee8cb9bbe8105658da856950ce8be6",[2715]="462ea36d446c2357e4bfbbc60e1cc346",[2716]="4bbbb037222d6578b046dc55cf0714c4",[2717]="67ba43d493e62a8fad5de319e6d4cb05",[2718]="67ba43d493e62a8fad5de319e6d4cb05",[2719]="67ba43d493e62a8fad5de319e6d4cb05",[2720]="25b9ab0087b742c3065f8746d9baa9db",[2721]="30457ed142330bc06a1fcb5a60aab6f1",[2722]="aab3b8470b8344f752edbd99f5158c6e",[2723]="6ba8086001bdd7a6df8594cb233adae8",[2724]="6474b107c5c183f7715d6a7e9b9bd040",[2725]="e09f049ad86efeee26a6cf070af34439",[2726]="cf07f03a87da49a65803d31558991b74",[2727]="029fcc802136676758f468a2de2e0b16",[2728]="4d66425225f3322096283b515ca1410a",[2729]="10e423272d76be11b815978a19589f07",[2730]="d5e73a2c013d1ba701af7eb660b0afcf",[2731]="757ef95b1d968c9ab2d25e37ab4f0c32",[2732]="964befb6e64d325b7a475de2df943ba1",[2733]="8b52e02e56afa09ed580667a5e55ac2e",[2734]="029615612562132c24664704abfb99ab",[2735]="e6920e77fbd3bc6df7b4506f02962894",[2736]="1aa81b1bb040e18b160bd6f0b5c0c928",[2737]="9bfa5a3c45ac9b01e0416e65185d582d",[2738]="c44b6028aca08cb9d13b01c12a2a94db",[2739]="999ce28b1f1ec225acb249fb240bf308",[2740]="8a454f45ca6d209aebf4f72c30fd9707",[2741]="3d3c34d89106ce3d88766ea6556cddb1",[2742]="8dd0e3cc687336207b4d925c5b493e22",[2743]="357c2ee51f3722515c63a654d6d05aeb",[2744]="e777e31ea17d1e5b2ab9606373bcefb1",[2745]="ddc745b9aece34acc3df75e63ad4bed1",[2746]="5646441fa04af085212c850ab4498c16",[2747]="1a68bd9a7b1ee66ae1dfc360043ccef4",[2748]="bac9b3cb652a9465386c40a24c916f1e",[2749]="bf7b4f0669f45a586bd429e0977bb35e",[2750]="d9e6b097fdb9b2881371f6cb40f69869",[2751]="ad404443058b0c82e860492008b6d766",[2752]="7d9de3e8d8d688383dca4cd43c9b820e",[2753]="67ba43d493e62a8fad5de319e6d4cb05",[2754]="67ba43d493e62a8fad5de319e6d4cb05",[2755]="67ba43d493e62a8fad5de319e6d4cb05",[2809]="890d68746a10f2a7a9d5060f707d6cf5",[2810]="4caa033ac9489e41a3716adf3adb15a6",[2811]="ffce96e3b08cb3c1f690e06680025be1",[2812]="dcc13b1fda93b46bd9ceeb1e975bcbe5",[2813]="584cde443c48fedcb831b10a21bd3397",[2814]="dbdfec364a84b249fab1dba33d58a521",[2815]="52181a2ce8dcb073b886523f51eda746",[2816]="f8973b4c98d0227934cde4920762b530",[2817]="67ba43d493e62a8fad5de319e6d4cb05",[2818]="67ba43d493e62a8fad5de319e6d4cb05",[2819]="d3da1ee0b52ec4bb6491569ccd48030c",[2820]="33263c446b82ed7663c73922cf7f15a1",[2821]="28e77a349969379ed9a560a8b63121e0",[2822]="9fc837b277728bea23bc1c0b94217214",[2823]="be302b220c89a3f864ddf69affb95bfd",[2824]="c92c1f8990b54717a5da740cdca0457d",[2825]="a604026e2063949cc572f4aa942178b5",[2826]="cc32f1a08569553e9828689f3608006d",[2827]="dd4ece4a977208185c8e4322ff293fee",[2828]="Kalimdor\\map28_28",[2829]="Kalimdor\\map28_29",[2830]="a50fd6b21631fd210f823fbf9a2481f7",[2831]="c2c5c85aa4bbef8a9b4d61d6090643da",[2832]="0bbdc261f16324ca0e825c3d55137853",[2833]="c7fde3592730251e32d05d1525129278",[2834]="6ca3caea5440c013b60d9c265673eed9",[2835]="081a3029393cf73a6a920afb1d816326",[2836]="3d41a61fa80e1dd7532afc4cba2433d5",[2837]="b476f2a74354c7be076b0c01a0408db4",[2838]="d9a45fe2625fca542f1a9b0affa93817",[2839]="fddb4a0d6cf8d89088d83f82c6b16022",[2840]="7215ecfda03267e5207277b13697c9dd",[2841]="3c5f2c94ca37b707c4f09607ac3259f9",[2842]="5072be66660b313599be0006364d18f1",[2843]="2181f198fac87d826996e266d294c242",[2844]="d7c4948cca0220a5b596584bb981489a",[2845]="2cf1ba7647f352374baaa33225c86890",[2846]="5ccf280485f368edb9a4319bbdd241b3",[2847]="e14745b7a0cfcbba8752818ac5750d63",[2848]="9ad85cd9706e75041e1dc2ee77cfecaf",[2849]="002684cf182f63c699c5d45d7e3cc48d",[2850]="6d45c4f8afb7c1368404040dd7b71b54",[2851]="9038eeca2501d7582daa4626825cec10",[2852]="8ebeb26dbe968f913566f2f7b4e6f4e3",[2853]="67ba43d493e62a8fad5de319e6d4cb05",[2854]="feeba2545612f8c620eb5b8891e703d3",[2855]="614df735eb063557fce62695c6b952ae",[2909]="e2ab07fccf65711098b35b6afea2b03f",[2910]="ed071031f096a629f640bc8d4970568b",[2911]="5ce08b77c73c620f66fd09447975555d",[2912]="64c8e76fa658e7aab8464e2cdb934b54",[2913]="141d6ac3b10a814ba4a72e8bde376ecf",[2914]="5e8a845ccbb630c647d6c8870d8d9f80",[2915]="e14da72779380aa8bb55c5ffa7672e70",[2916]="40720a67aff65b7fa28732dce51c928c",[2917]="2865e4e284a1f2c6b9201af35f68e060",[2918]="1a62dc0549d8c0bc1f13e99672518885",[2919]="07e8169bcce4258757d09ab43707d092",[2920]="1f1156b29bddc53e87f547d6fce72042",[2921]="143900ab4db6caf28554fd1077b010de",[2922]="ffc2546657463d5e717094d734dedf59",[2923]="42df3cae5bcafa5a080cd8a5e6d398ee",[2924]="e145fdbdca6ee81f65ed1e69109d9ed1",[2925]="a2f7d0f403d47faf5b7dc2011709f113",[2926]="fa36dd6fa8bfad6e3f3385e681d01f44",[2927]="cd1b8ac60d272fb615cfa87b6826edab",[2928]="Kalimdor\\map29_28",[2929]="Kalimdor\\map29_29",[2930]="aef0726e430468c85d7d11efcb6b1b60",[2931]="9fd0c2ba48a574511270a806922b60a6",[2932]="c86d15e10612fd7a2bed736243d27b8a",[2933]="f17d69618af9fc2c742c378062f90a33",[2934]="38b5375f311de54c5455a9f12390212d",[2935]="3ef6e34258f85f9683823d1fc46b6a11",[2936]="a8fb492bef8edbc979c9d1721222da5b",[2937]="2f82a8819939c88e12c33b4b6e6cd95a",[2938]="c0ccb93e5207a0c65b32e644c3189ca2",[2939]="b56901acc6c8f4adb1f474cdca3d97d8",[2940]="080be8cd247f976f81f7a8c5c9b32e21",[2941]="1e9306158984518a8d4eb1e7255a39af",[2942]="2489f23ea78d7878188c1d9b7919c80e",[2943]="667da304b15ca467831d91846cfd8f73",[2944]="ff0d0951eb2250df414dc1711b40f90f",[2945]="1d9cb49e395de2a398355fc639b56534",[2946]="7ca3efdb12d43df1c8cf89352f36c754",[2947]="e89239e379b30c71100bc1449f5c746b",[2948]="bd35951753c2e9f3f550ccbef091c3d9",[2949]="87dec3c086d74764f45d5ea73aefc7a3",[2950]="3d1394fa3bb3209ccc66c80dd15ae0e8",[2951]="d944c6306fc765ed52c4980e1f3431a7",[2952]="a55ea2d04d455ec7e7a396d8346fdd1b",[2953]="2b5bc62fc88b80b8dd7327e07520c076",[2954]="40dd2389d45b0e79aa1b1207c33667e0",[2955]="f248e9bdc27861bd2a7821584cdd3574",[3009]="fad0c26a31d7d5ed6f53aa158734fd7e",[3010]="e49b2cb067cb835e8ec662fd78c094aa",[3011]="5c03b6b8a3d9389def0ce740af86f040",[3012]="c1cf61bff7953072a4b4160938524698",[3013]="5b423fc77c917a56ce4e21b28f1d7cc5",[3014]="d54603e8e61705f3ec7a94099b0f2ee9",[3015]="42a3dac84522b4e1b3e32723ab1416b0",[3016]="79f057c2931839fe703ff028cc582efc",[3017]="341f394514ac97b21b29b0f176c270b7",[3018]="4f0803405d70d0b291ef7bfdff7d1e1b",[3019]="067cc5996a314baa6bc705c073a9509a",[3020]="c6613b9909c2849dabfd599322b51cf9",[3021]="2c8ce9963f98880fb131bb97b3462c13",[3022]="4f2fcc62faa8386fc283ad1074f1f34b",[3023]="83b404a40884221465e2658405bf9797",[3024]="253d88eda8c8061fdf4547499de47703",[3025]="e4f973c411b4e8fde46345ba2fc5dd0b",[3026]="5f385dd01dedc9fc025cc2ca78bfa663",[3027]="84839a193a1d885ecca5d3508313ae79",[3028]="feb33bf2a46607b789b668a34e4296ad",[3029]="124d2766e3bcb2155b9140499e2771bb",[3030]="5f31b9805d021e9e990c0e282a50a890",[3031]="7709d701c046cd0ee87baa06dad0d00f",[3032]="8b6de5c077db08f98d5d45fed88e4c6d",[3033]="db9ecdcf13ef6bb43c9ac1565f65f452",[3034]="0936fb27690d983ff34440111d3f027e",[3035]="9e634a79dc3cb804a31a43341c7eba74",[3036]="761cc23f1381f3e1efdeada10d3fee1f",[3037]="fae499cd4c7fb15390a09712fe0fae3d",[3038]="9e069e031ebff940fe883c7007df4442",[3039]="df1bc6a73008f8720fb286b08fb48ad6",[3040]="e97f45a44c7489ac524e34bd5801e33b",[3041]="902d8a3e7c05fc0d4daad220d42c84e1",[3042]="33be2abecd5a4c0fcc52928e3711463c",[3043]="b3ba241e5ecb94866209e9fe41c4ab91",[3044]="c5effe2085e08de019fc53fbb6d0aa41",[3045]="dfc0d5034c9ffaea8ea802033a668f9c",[3046]="7fe231246255012dfd1ded84444e3d83",[3047]="02e7e6a2e1c9ea6bef3f958427810c7d",[3048]="fc3fb316f6430cf5a7e12a5a01e40ccc",[3049]="e0acbb43ad9a066471f164712077abc6",[3050]="f69e87af0a4d8e866cf0e3fc3b857840",[3051]="81aa9e53f3812dbaf1610f0c8a0f6683",[3052]="a3e243708f47a7ad986da6985ecfdbc1",[3053]="9da612d03567f159bf7e66283e3b4cdf",[3054]="6a10a96d079a6ff84b5c71fd68446510",[3055]="1400fcdfe2ca0858409f60596de08065",[3109]="f991603591330a56013738343edb421d",[3110]="591a88e3d98b7cc4ee24f79cc32b6ae3",[3111]="4aa927f5dcb8ef81950603a383b94bec",[3112]="850714b94c617c8042aa3243e76f9929",[3113]="0c68bba4c37c27f175d46c43703833ee",[3114]="8b63956359e1b90ad28d4cd56fb35376",[3115]="cc5472569d5913b6ceb122687ac2f2e7",[3116]="a6a40f0f9349b8e122fb740b7c9d74d9",[3117]="5bd73861fcfd1cb96d69e4d8c064232a",[3118]="f8fa04e59a648783526725b5d8f68614",[3119]="e3e0b7ae1b48d219821a574070fcafef",[3120]="e4613276e89db9e7adb061ded517436a",[3121]="3def17cf35f5d823621b2f4489e3dd34",[3122]="bea08f1999c7c86dc7d6f5d9e5410166",[3123]="ce93f92ca53890e5d2f701ad932db331",[3124]="4a1464dbb78ac92cdefe6b5c7414be5c",[3125]="105ede3a9abf660c8d91042424ab7d9e",[3126]="db75bcc3fab8b2fd1e35acb8e8f390fd",[3127]="05ad71ce35b27b3bfc2e1881e34489d3",[3128]="2e23514008e63154056e8d2d2646de7e",[3129]="98ad8e2b093da8a5b20c03f97f0895af",[3130]="db80b4975e3016b5414f2050df627cca",[3131]="d28ae4bb0c5174512ef4cb66adf54474",[3132]="a6e5bc9a7c640e15091ef9c737d04903",[3133]="fd1619f16dc47b7eb3cb8cdf4691a3c4",[3134]="500ccf10096766a982779bd0909642eb",[3135]="f5dafdef83cc2ee1bec3e619fa4fd2d1",[3136]="ac96e008c5e048ad80d75e57aaf66023",[3137]="091f28cfe892ff5865d007565c636ee9",[3138]="294cb499c2f54d42024344beb7de0c9a",[3139]="94e7be2f4563c5d61c28f847ff1286dd",[3140]="7ff05eaa596cbc94783a2ec1e7c7e84c",[3141]="cb7140c3e38e9c9f3a7603a5de605b76",[3142]="e6225e8ad4c68741408821c1fcc4244a",[3143]="9c258b5c8478f112a8a2b78fc8a8cadf",[3144]="6783329dc22e046561b8bbf85b7268a1",[3145]="a1854e99bedc89745acee641816b2cda",[3146]="93b021c5b05d2eba0afc3a98747bf97f",[3147]="d503ac7ac10f9b38bb210302f4112368",[3148]="1689001c2a57ac542fd3f55ce1a9a705",[3149]="9d8516e66fdffea3376cbe4a720d5159",[3150]="d9cf07a9768c525d432957976bb74570",[3151]="b1ebfa0b2c471a2f244c06a4976181d8",[3152]="f529062f88abac9339fa5c16d4c350a2",[3153]="39072788a38a448c6ef1c94aaaccd390",[3154]="1400fcdfe2ca0858409f60596de08065",[3155]="1400fcdfe2ca0858409f60596de08065",[3209]="1491fb32ba4237533b7ee0a14cccb314",[3210]="e9a47ad0452f24d96f2b9759dd574699",[3211]="fbbd9be9d9f5e3b733215ba2d315f82b",[3212]="615c0997b3a9aa5eb53e0da648fee2d8",[3213]="1cda9eaca0ee5224e2eae7f6c2f39f6f",[3214]="65d2119be80ecaa252449e686f0771b5",[3215]="b7c4bae0436e5074ddeb37d53130f07b",[3216]="86bcce995ca29294d43a5e65ad18239f",[3217]="dd1239a9fa1d35589892e7c9fdd5ed87",[3218]="e10b5f8768e4d5352884e7b5e086f634",[3219]="9349f3c89bad4899200ec1db030c9772",[3220]="0d8300c438aa1b9745892a3955157b7e",[3221]="947c911c35fe67a1b2513c742b226de8",[3222]="60c1985ab8b1fa1fd1ef4e9e26294836",[3223]="fee61cdd6bd04c12849f2bd87ad6b29a",[3224]="075e37dc7192288d4759ab5d3c39274f",[3225]="34c89bb433b47b66a2d24cbdd416048d",[3226]="88e2f7534742fe10f2ddeb355f130e4a",[3227]="f9c8df3a425700e805d34ce24c72ed4b",[3228]="9af0a97f2d1924e9b96f18e28215b28e",[3229]="d92192ca9387edb7f8f3552dafede697",[3230]="f1a679c93b3711fe6d661d9675d96b92",[3231]="a8248642c0b41cf98b82c84c7b033b77",[3232]="c62a0a4625bba6cce8db2d0b3e2312b1",[3233]="c53681a36f8bcc24c0d4fc0b62c8bd14",[3234]="c24a7f96bf1f9fe715e314fffd6641c3",[3235]="e0af5322d6102982fd10407181fa7bff",[3236]="b5fb8e3ae18e466ee359fd6cae02730c",[3237]="60874e5007c9983e447e6f779df9aba8",[3238]="f9f1e53fc8ba86c2e5c1ebad73446ca7",[3239]="b59ea025bcc086345d1d1944f623429e",[3240]="Kalimdor\\map32_40",[3241]="Kalimdor\\map32_41",[3242]="Kalimdor\\map32_42",[3243]="a2edb1361aa1edf0c8f3525e6c5d0c34",[3244]="d1c4fb4b597798f1c6e44b0094b0c638",[3245]="c184655b6a359f9c6fbde4f341c714bf",[3246]="330970d96e549480d651a103e002a698",[3247]="f8e0af08b3f511db96527e468ec23a10",[3248]="ad2afa25f238528f9e016300a83eda8d",[3249]="668419fd8c6ed9777c49bbd9ffb10fdb",[3250]="adcd341b90c493fdae6f6896d182393a",[3251]="c1fab3e02bf06ddb863d4d055e24888e",[3252]="c05ba9adc522438ebbc936db54cb70cc",[3253]="f9a500647fc01d679a9fd3532aae15fe",[3254]="1400fcdfe2ca0858409f60596de08065",[3255]="1400fcdfe2ca0858409f60596de08065",[3309]="67ba43d493e62a8fad5de319e6d4cb05",[3310]="67ba43d493e62a8fad5de319e6d4cb05",[3311]="fe8756969de8fc9c71f67a5c3636b68f",[3312]="0d622ad53fdd7c9216158943c5c81022",[3313]="424b433577b4fb4b2f571188482b16ba",[3314]="8662fa2b66eca6f44ab2be216d9adde3",[3315]="c8f277290f2363e18412f2b0ec8d811c",[3316]="765f96c1d59711cfb8a367a19e3eb88b",[3317]="e6fe092060ed57a896a25c0f79297cf0",[3318]="22210610ddeaaf841f9b249d31eb2d90",[3319]="c9d8d1896f36cf375f4395523767e873",[3320]="0dd9909515b2d267924b29e39c2a1f91",[3321]="5c2e95d4579d4fead513866739f6fd91",[3322]="0ee3788409d7e3edfc8e10efe7b1e491",[3323]="e8d57dcbe3237c506d892c5f249a29cf",[3324]="bf486a68dafcf69ab094ea277f8c90bc",[3325]="2219ec655ac810d5d4937389a30d866b",[3326]="b728ecd5fb073689b9167f16df259161",[3327]="dba85f757e170454aa5e336bcbbe5f58",[3328]="d7dded3d7f09aaa9aa315b8dde101aad",[3329]="Kalimdor\\map33_29",[3330]="Kalimdor\\map33_30",[3331]="8145982f925f7f764b7b06f3231dafb5",[3332]="c6d33e7660f3280ad5b54a043bdf9888",[3333]="516c292f47b3d49e8ccbf3518e85fa8c",[3334]="5f74881e2e68078b03598c09d1fc266f",[3335]="29206e4f40c8acc097d0e85494933243",[3336]="1d298feb2b1e0c47b8a4f46ecf314de3",[3337]="efdf3cba7a0266d4d634a32b7a9a20ba",[3338]="57caceec7486b6b1308c10f69caf7aae",[3339]="acbc9ac4a1b39bfa86d64839150ae603",[3340]="e6992e8dbf24aadc80f51e261a2adf49",[3341]="cc444956a5a8431ae13afbf1a2551550",[3342]="8a0bd513b5c5a32aa9365a967ade7721",[3343]="5de451f810cab231187959a63b1a814d",[3344]="10ff3301e4a225c93c6e6f3b921b50a0",[3345]="4bbce36166073b1d0ef3eb443e8d228a",[3346]="3b19ef5038bf769b8516bea901893ae8",[3347]="683f7f098a8c3b1e4aea27a0f14ea1c3",[3348]="aab63a5bc71734c4e72a9fcc3cf2cec3",[3349]="83f1a22681c8b43695cf90435971b7ae",[3350]="3e3011c47e2f69af90c1e0152b6a924a",[3351]="ea99171ea3d98fef7b12c0ad579c206d",[3352]="51b769ee8c2cfcff25377978301fff78",[3353]="ca40bf568f0e08814c95406a654252d1",[3354]="2930aa980eae7de26cfccddf2e874ac6",[3355]="1400fcdfe2ca0858409f60596de08065",[3412]="67ba43d493e62a8fad5de319e6d4cb05",[3413]="67ba43d493e62a8fad5de319e6d4cb05",[3414]="67ba43d493e62a8fad5de319e6d4cb05",[3415]="8abad848b354434470367b3772d99a48",[3416]="44db3a93699e6fe0e040faab7a34364a",[3417]="8d99d33e900b61111a6981c83333d48e",[3418]="1319f7e9ef77fe999afabf113caaafee",[3419]="f493f7478dbac6ee3dc499efb35a667d",[3420]="20b1eafbd1b62e0738b5050c34629557",[3421]="f842df3fe0b0c0742c3f43d852879f1c",[3422]="88c47b0dd034d02ee1564f5eede826fc",[3423]="312735836dc2f007957c13b2ea45c19d",[3424]="84936d2d1da604a47e5e4df47c88dc75",[3425]="3293cc327b634a54a4b87cee84034b49",[3426]="f4d606a73de3253dae85bfe940f3f66f",[3427]="504a7d0bb6e1d6f27ad43019c2730b57",[3428]="01d8fc1480424b01a168b0b2a5ed2836",[3429]="4a3d10241b608c013699486c71ddb1f6",[3430]="0f06da7cc7b64ad899194af99a0f4068",[3431]="8ec0d75ed964474136c6023dfcee85dc",[3432]="c90b8f39de81e8581546a7abef0a4c8c",[3433]="b479c7b04ca81c4ce0a66c172b431890",[3434]="d5ab41aa0bcfae1c84a48de22bc9b7b1",[3435]="93bf72ed2a11f09a6715f25ec38a234d",[3436]="df49ed7473635d4c094aee1e8ded3cb0",[3437]="015c7ec142a2dbbbe7bf37fc89a1d9a8",[3438]="bff10a194a9e3f0610fa49db92d275b7",[3439]="8d1fd2593dc18538ef266553eaa68625",[3440]="f020b01af4de49e4c9978f1f836aba3f",[3441]="fc18e86e8620827e3c38257b308fd76c",[3442]="b5c1325f6da34a7082911890826e0a7f",[3443]="ee6966ef56d2fdf5cef5471d2ee5cefb",[3444]="bf0321ce752c1398bf6692762e271729",[3445]="ec5a182e57c8601d38d87a8bbc133644",[3446]="39c3f6ef5a1610449165933440b9257a",[3447]="42c01e8d0208e2478540ccd17368bcb0",[3448]="b03cab9f830de92dee77a0b80eb7f5da",[3449]="625d159504bbfcf1f250ea87a632cb8d",[3450]="67ba43d493e62a8fad5de319e6d4cb05",[3451]="65194d2e4017068b32703873225e469a",[3452]="91f145d0c8bb7216b42c2f1c86ec4421",[3453]="884f67bdeadc1ab518611bc8741aa686",[3454]="8ce63197a12dd857dc288da2a5a03a2f",[3455]="4ab07ff5f5eccf744c90180aa42772c2",[3512]="67ba43d493e62a8fad5de319e6d4cb05",[3513]="67ba43d493e62a8fad5de319e6d4cb05",[3514]="67ba43d493e62a8fad5de319e6d4cb05",[3515]="1ca65d71f6cdff6423438ef678a7062e",[3516]="9c2f2b4b9440849153232ccfe5385719",[3517]="6c27bfc8dc5b4b8cacb2643d3ef0bd93",[3518]="d2da7c37369c3d1b2d4ad5b80b74e4d4",[3519]="aa6f7436cc4620b92a1e70db70297bbe",[3520]="b24b703d17cd220bfcd1f8612b812b88",[3521]="0e810f438620b2fcf8d5dd1159445f34",[3522]="a000e886f413f6c128e3819b47cfa972",[3523]="ec152c512f2a10a52eb2962ad43d7173",[3524]="a510164968aae73d547310d1b6033459",[3525]="b57d1937277d61e7ad961d35dd7c1cad",[3526]="ee820413b3b80c70d1a3f0876a8bd4f8",[3527]="ac4aa9994ee7feec4aacb74032374fb3",[3528]="02e659af778ff32f4b7ea280ceb8faa3",[3529]="c0594d6c862b019d23a9e81124030b90",[3530]="97bc560cc8cc67d46fb13cc8ba51dc1c",[3531]="3d9fd1b6f91e2063fd58b924d3585232",[3532]="bc9fa1dfbaf8eca2242c400d49a80838",[3533]="c092d1db70990e17488f097d40529837",[3534]="28b15fafda5fc2e80426bb05a80df799",[3535]="46e392a278756f435c29127ea854bbb2",[3536]="43f2a50298e18c57dda7af7a38d18c46",[3537]="691f68dabbd91b950aabd12278477c20",[3538]="104c7a2356022ce37e4db87ef4180b5e",[3539]="19369a386845c4c7dea057184847496f",[3540]="f5cd47561727ac8d6715ff08167778d8",[3541]="154ec460682beb4ead7e98afac351f3c",[3542]="46f7fd163c643a8bab608a86b4948457",[3543]="a93e47a89494b159af96e4ed0b072637",[3544]="ab26b98066b78a3dc0d2c63b3042d43b",[3545]="6dfc86a869a01642a1674485cb3ccb0e",[3546]="3b492232adc096eaa47cf8266e978015",[3547]="4938a03304747b7ec2ae1e8ab43d48fe",[3548]="25889220c428e384811bbeab97026539",[3549]="8a090eb8855cc7ae306b703e3b3e7fa5",[3550]="58ca589cafdb1c3da0b049e64051e351",[3551]="fa6ad77d08492a5815be40ff19fe18f3",[3552]="67ba43d493e62a8fad5de319e6d4cb05",[3553]="67ba43d493e62a8fad5de319e6d4cb05",[3554]="e31f2ba1ca38fd2a4d90f68a37911f18",[3555]="8cbade61f105dd541a5c7e0f81b6ddbc",[3612]="67ba43d493e62a8fad5de319e6d4cb05",[3613]="67ba43d493e62a8fad5de319e6d4cb05",[3614]="67ba43d493e62a8fad5de319e6d4cb05",[3615]="9d8fce7d735f513531b705c341da665d",[3616]="42158f01401119930dc7a7255251109f",[3617]="7002274a939f0d64ebf763bfe595eb42",[3618]="088757201e0c7f42573b52bd5a6587d0",[3619]="33dde804f07658afeaff5d23afae8821",[3620]="4ae79440154d4bfa1f58c85bc0039f8e",[3621]="84e7d77085232ec02da88429fb60e1e9",[3622]="19fef36b8911de4c2730865da81d9b54",[3623]="a98e3dd7196f6bb31502ea7b488cf8d6",[3624]="fda63b97f7ff47855cddc2b0cdce3aeb",[3625]="521f781fea80a9d9cffe516d03d332bd",[3626]="54e8bb68315078f8df1a0ec904bf63dd",[3627]="7f6a56c3ee98bab92573f8cf80fc6317",[3628]="dba235156e3c28873492cdd81496cbdd",[3629]="04a9c040e479621d2e6fa3b18fe02678",[3630]="f2a37e66f57b488e24df1e776431be5f",[3631]="65e4abe48e82c471ad9ed6e8d7e419ee",[3632]="d7de8bba72646300939b3d97da074496",[3633]="27c07f92d7e35ec9e52291b9638d379e",[3634]="6135ac9fb4878a33a959f09e82b1b3c5",[3635]="f0b6b6dba323aa6f6a8a53c7dc10ae72",[3636]="ca48d11b80de40946b4cc2eb2b614f19",[3637]="75c4cbae07172ae3410eb637a298b3fe",[3638]="fb8b305bf9043a9d062fb57d01670a19",[3639]="be0529395022973f75283554ca50a7c2",[3640]="5422b529544b02e5e498cc9eacd45aa5",[3641]="61322e0b051fb4d4712e5f7a18b678ed",[3642]="dabf76a761a0345b5b7adcb030cf4f14",[3643]="a660b7b60367f2322c67ec5488a680c6",[3644]="74a3c681d0cb78e11267fabc6fa4b0c1",[3645]="172bf9d9a180a6bccd9bb0ab5e104ada",[3646]="c71fdf3cbe1deb1014b48fdfd563a10d",[3647]="6fe5a52121a2c668c2760ff556512911",[3648]="2ca8dff9b3c08c0031390db24f2ec94b",[3649]="fdf1734b84f54f336580a7c779a4acbd",[3650]="2bb52ec7aae7735687c76f7a03ba9fdd",[3651]="1c658e94ede515d014ca3da2c6b9a406",[3652]="d9739dd741964f8642e5f5b35de306d7",[3653]="fcc6c561d6576241addf4c6cc0301dae",[3654]="67ba43d493e62a8fad5de319e6d4cb05",[3655]="67ba43d493e62a8fad5de319e6d4cb05",[3712]="67ba43d493e62a8fad5de319e6d4cb05",[3713]="67ba43d493e62a8fad5de319e6d4cb05",[3714]="ee5cd81ba2561b42565384be6427c797",[3715]="b7f860ceee390a61b4ca0fda60a99ce9",[3716]="7b973b65f41b2f365c82c88aba6908d8",[3717]="6c5e545b921b51dc6f5a47c99ef9a51c",[3718]="1abb8c7f12782c0b720b0d360c93f19a",[3719]="03d9aa622fa32e24882726f38fdfc276",[3720]="478e12a8e9fc63c1a8309924e316dbfe",[3721]="fdc53e4b7bc1b51633ae4365cc3df93c",[3722]="aa0d202217034cc8f747979690cb0a11",[3723]="08079a8117a4ddc922366792ddff0934",[3724]="82e9a4a8d1faf2f4e934fdc46e77d2bb",[3725]="e862c9e5a51875a06652269c5410f099",[3726]="da0b778f06379e4ed2c50697f7f1e8d9",[3727]="e890d156fbdefd7496d3978bd0572caa",[3728]="ccbe693c2d0a653c359b149fb79585ed",[3729]="71597fe776f9edadfe34fc5969f84166",[3730]="b5e34d8a1912aa390c3703342c54e263",[3731]="ee56b9a821ec5535cb564e7aa3bff109",[3732]="76a9fb035aea34ce54cb05107c19fc03",[3733]="2fb9b35d1cc006bfc98bdf4ade68ea63",[3734]="d4413d1f5a9bf9b7580f86570ff93206",[3735]="bcf263daf4cdf7d947c730b67bd51563",[3736]="df67bc13dd7900265693ed26aea0f685",[3737]="12fa2159a172245c2953171137d9fb31",[3738]="36eb556ead8bc1f72b74b6112176b8da",[3739]="833cd82bc787cdfac7d5f01e68c14159",[3740]="86ccc640ebc919d796536e3793916ede",[3741]="38483e50e0b1fb1ee683fb74dd884e6d",[3742]="ada54198a4591020ffe8426c702270b0",[3743]="f9113999f0cf3b11b0530d47219e13e6",[3744]="dd62b3d256c99871676dc0c56ae87ba7",[3745]="7a0acb49bc771a14e7bb7fac01575240",[3746]="93e4e0d2e1449c59b15d2dbf910bdd19",[3747]="2bc53433030e2e5dd563a050c0d83b7d",[3748]="ff0462a05138732016cd0076a6f15b51",[3749]="522a228cad63934b29e57db5f7aaf840",[3750]="d4625862eb9d888e87a7b846700fa95d",[3751]="f9266b7f9822e708467ec620d5622d5b",[3752]="0dad7c30868a9cfd2c69f573f52cdc4d",[3753]="79fc145fc002ac787070fed74fd266c9",[3754]="7f086a14891147a0399523263fe896b1",[3755]="67ba43d493e62a8fad5de319e6d4cb05",[3812]="67ba43d493e62a8fad5de319e6d4cb05",[3813]="67ba43d493e62a8fad5de319e6d4cb05",[3814]="5f89e74294176fa83923f0f5dc543d57",[3815]="04a2b78f36acbad5ad2f14040144a7e6",[3816]="d8ffc2d2ce0ff399ad5e4e5c24697778",[3817]="2d3f293805622d34d4fdf7e5815871a9",[3818]="db80827189688b0f0e354613bc50aeb8",[3819]="9c21cce6ec8f124ada4857767bcdf2e4",[3820]="a3dab705295858549edc07cab695313c",[3821]="eb2f05bb990784a166093e564bdee021",[3822]="58769e3c8b783f363a0fda04958c0ce7",[3823]="a2e56f92bbc76bd768cf999fd88763fa",[3824]="2dfe02190f17f7de375cf9bd250224ec",[3825]="7d01822fe4ee4d5ea12075707417325a",[3826]="b64ee84896289bf8b149cbec350e14f1",[3827]="c43d0166beabfe1a492ff8ea86dc81ec",[3828]="64d39494d1be04b2b6f81e8268af6953",[3829]="a1cfe9a67a0fea1c325c740dbaae9c49",[3830]="1aff912efe70ec091d66fa94072284ef",[3831]="f9aef0d5c5caf123577da4d89d989f72",[3832]="a9387c0525943d235e405f3c42d25958",[3833]="11ba67f995c5fcc7bc9705b05fcc15b6",[3834]="0483f1821b70366852e4f929275f9cbd",[3835]="5fa2cdc411c81021cd669558c7faa0ce",[3836]="e57b96fa109a7f119e78681f59f728ee",[3837]="675ecd879ec0ea9b7730a2716fecfcb3",[3838]="40ee1b7e8eea05cbe1097308d8c49a22",[3839]="a128a3a17997f5760ab93762537ffe85",[3840]="6ac47a51b3a9062d5ca9cba345adb103",[3841]="cde6e15e40c7bbd78c1c24a12d2c85b6",[3842]="01283653e5af801e424d4251fc58bd04",[3843]="baaff7fb8dd09ddbb068c008f8ee3cfa",[3844]="e778fbb7274559534654e74a421de3db",[3845]="5500d15a682f35b57667997bb9f10a59",[3846]="51487b51e221b6fbac0994a3e1093f63",[3847]="bf926a051b64fede7112a2afe3e8efd5",[3848]="79a71159e01e7c106cc24dc84efdc057",[3849]="d50faaaeb5e077859d0a79ba8779d508",[3850]="9b01d5eb29d07dfaa39256b56ba068ae",[3851]="c6053bc299ec0387c4babca4bf634612",[3852]="184ff52e9b639cd2b0588bbdad71fa22",[3853]="944d4c023d02cd9b779ac99c14566c49",[3854]="14c2e91bdb3a21ebb9f36a4038d8f169",[3855]="67ba43d493e62a8fad5de319e6d4cb05",[3912]="67ba43d493e62a8fad5de319e6d4cb05",[3913]="67ba43d493e62a8fad5de319e6d4cb05",[3914]="0669f2c10db23760efcc2d1843958572",[3915]="13021c616cf9935410682bf3450334d8",[3916]="85d5e83db31955ede911418e0baa980c",[3917]="4ba76513e499dd5d3b51527ced59cdb6",[3918]="f9d7e4f9fe26d7a060c77a2715310290",[3919]="79c82a7036dbf74a813a011f48464cbe",[3920]="ceccecc564109e66144108ec1b2a8958",[3921]="1e31fbb8f835e84489b18f8d3d42e84e",[3922]="048ca9d8a4425d319fa8a1752976c717",[3923]="d389c388216a95f9ab92578ddbddcb7d",[3924]="39c1a6679f6a4a4c15c76be77480050b",[3925]="7a3a571e712aedc556ee56e0f23c60af",[3926]="401c0a91ecd5117b5010c0f035f8cbd6",[3927]="cb8a9a553bf046accf2eb220a93d7e2e",[3928]="aec80f3723dabd89157943d246f047e6",[3929]="a11408b6bac0c0e9af7809758d2040f8",[3930]="13d90526389c686dc7c5e8975f33a889",[3931]="1860db34adb5c1367877e99f188a522e",[3932]="d68a2f2712907972845d7dfca460a5fa",[3933]="e8940792cddba3dd4823ef3fc8c39c62",[3934]="bcf8054bb437c4ae5df2f0d41903fefe",[3935]="68f5150e97cb23776aa480faeb31f8fc",[3936]="3a89285e7a50dca5e11d7916ef8be1c1",[3937]="3aafc806a0a3c66b9cc3a27766ff3ea1",[3938]="4c7df734b12741ee169ee2ea2508e589",[3939]="b3a52fe6b57955104d6fbce1684cc195",[3940]="c024b9e25139278f157653bedba94304",[3941]="ca3092b9dcfa90ab7b2a7406f520b25c",[3942]="745e05ef2aa02d97f160126e1079eef6",[3943]="c349a92e4d42d8e9e05106e9f6269ece",[3944]="99b8762623c896d85dfa72afafd1509e",[3945]="7fd9471238f9176f45d9362ff8433b55",[3946]="327edfece372684a771f596c4c1e65d4",[3947]="2e5f3dae491560341d2f62d02844227d",[3948]="023e36d7b2836ba7223fe5ba071f1ded",[3949]="556547909c6ebb97f9e475dea3468853",[3950]="6b178f10ea56208879d97390327ec69b",[3951]="f2fbbfcd90e41890c96c4ce8039d66d9",[3952]="67ba43d493e62a8fad5de319e6d4cb05",[3953]="67ba43d493e62a8fad5de319e6d4cb05",[3954]="67ba43d493e62a8fad5de319e6d4cb05",[3955]="67ba43d493e62a8fad5de319e6d4cb05",[4012]="67ba43d493e62a8fad5de319e6d4cb05",[4013]="67ba43d493e62a8fad5de319e6d4cb05",[4014]="787c53043fbc908bc533fe006af0980f",[4015]="33bfee175e690e0217dd7c226152c245",[4016]="588e42bce90594174a028e5d4369fbd8",[4017]="ba679466e2b3d3ed1d29136f2eeb4578",[4018]="3e60045a67a656b37555b704bf99e9af",[4019]="b0124ba63aa064eafe30b9870f39aad0",[4020]="0e535ff39c11a35da42c341b73d2be03",[4021]="95c026b0e7981b7cc126f061b0567aef",[4022]="aa67b1c37adf536744d73ace8e54747f",[4023]="f6307075650ec178f6a08f2b41fb6836",[4024]="63872041bde974d0f1413498d4b52a15",[4025]="9f0fb02b6285750926e1c585889b8b04",[4026]="f81097f988364833b2e2c4f9fa75be27",[4027]="f4ddb6271f3c2146b01737c01a317dd4",[4028]="5d61f89be76e7cde539816e082ac252e",[4029]="97d3bbc464589877d93aaf159234ba6a",[4030]="4ba28439b8fad5f3a28c828ed6e1250b",[4031]="7055956b29005ef6b3aab4623337e6ef",[4032]="7310c8e232deddd2c32be66add926488",[4033]="5e74f8406fbfcff9c79a7d333a6f9942",[4034]="aec3264917285f2d62d2b76b666b83dd",[4035]="7be7fc559f30174659d89eb528ca12de",[4036]="5619047e213c4e505b615305885c1f9d",[4037]="71d63ad8fec91957557a4533b51955ef",[4038]="badaa487cdfed63eb87aa059177d2577",[4039]="568d4ca706b5c8f0edff11dbc5b3a122",[4040]="33c17c5bd7dd69fd7011b5593653e5a0",[4041]="ddc710f4690e8134b1ef280fbb9f068f",[4042]="c6d20641711bdfd28f8628093e5e037e",[4043]="3434655a06df6818edae38d5721dbdd9",[4044]="f5768edcfc8ee187cf8d041bcdd70ec4",[4045]="9d123c35b75c5d6b04e4a0428fbf7dbd",[4046]="4d2c9efafbb17397aecd384bceddb030",[4047]="8c9ccdd16c6ac28751f23d8b9434c21d",[4048]="753e79d921f148ba44871afa5d1b72de",[4049]="41c0cbc12482c16525ec335336c379fa",[4050]="2fabe0bb8b78d2ec424f1701178b836f",[4051]="9da20226630e12b93ba0c402874c390e",[4052]="c7e898aa5d29e63e8a02e0604bd83e33",[4053]="90303e559e3fb8c2dc36232475e5ede0",[4054]="5de27ccbeeb93f86e1cb0d6169b2056c",[4055]="67ba43d493e62a8fad5de319e6d4cb05",[4112]="67ba43d493e62a8fad5de319e6d4cb05",[4113]="67ba43d493e62a8fad5de319e6d4cb05",[4114]="67ba43d493e62a8fad5de319e6d4cb05",[4115]="d8909c0d30dd706eebac08da0f308c01",[4116]="6209f096c2cbd18fb69c633d209d9f70",[4117]="8ccc3dd876335254613a91322bf19307",[4118]="95556b1d0fa3db8e44a26afa270ebabe",[4119]="54286d4764d00bf0cb28c8ffb8c33971",[4120]="38296806935d503bb6f82117914e0143",[4121]="e414af8a0866098d247acfc291dd25ef",[4122]="0afe701e351413fce12d80d64676d3e6",[4123]="f17cc641b3ce4b0f17029f39d19853af",[4124]="6153d2747e575a5cb82bbf02b0e2b6f8",[4125]="35bed9b6dc8d9626d9704e6956cc6b57",[4126]="1a98718964cff4181c0e77aaaed12372",[4127]="b4b9e1b809f521cdd560308f5ecb53e4",[4128]="71a62678945fd544c97e57f0e4eb06d1",[4129]="d85cf91cbfaafb24903e9935d0ad9a1e",[4130]="cea5374d10aea88b39f1fc5de18f3f68",[4131]="0db201c2e6b714541dbae818a3924c50",[4132]="b93ca63f82785a85171c415655f6a35d",[4133]="44f0e6418dda197e2af7767b4e59243e",[4134]="0204fb444076414db9b111beabe23613",[4135]="0a6fe79194847585d042369ec9192b49",[4136]="a833c655e55dde906b9c3f22d6ebf245",[4137]="7794e83befc97286b20d99891cae0bac",[4138]="a454ae70f662c79810af2729b80876df",[4139]="4888afbeb25d2567feb309c518d059d1",[4140]="b6a1e7a5a67e4f97538b06a63eb2dcbd",[4141]="e8106095a0ac5e69b9cca92623c88348",[4142]="2cfb3fe1ada7a9b8b819eb703bbe5e66",[4143]="88cc3219aa12180c08764eab8b6ca63e",[4144]="764b2510b49d352a09ba37a90307972b",[4145]="b4a64d06d118a73251fa3baf5e333367",[4146]="e9be4feea30895081e112ceb415dd7f7",[4147]="9e2d565fec290bd5d4978fb825d0aa5c",[4148]="8f5eaa897e311fc1e4b6d699751e1805",[4149]="beab12948258afc1de6eb1c7602e4dd7",[4150]="0515828bfd80759c7183481a2b7ad919",[4151]="1a169aae1b771aff426a3aa5f08e4690",[4152]="18d6fb6590bb7f47b8518b2e53efc609",[4153]="e5cbbf4a445776076d2f31981670da69",[4154]="9d74c9c4bc60976ef912dca1c9771f9b",[4155]="67ba43d493e62a8fad5de319e6d4cb05",[4212]="c9371a8d5d021aa58e08d39acf9aad5b",[4213]="c9371a8d5d021aa58e08d39acf9aad5b",[4214]="c9371a8d5d021aa58e08d39acf9aad5b",[4215]="c9371a8d5d021aa58e08d39acf9aad5b",[4216]="aae4d55c3d07f15b5394cfe15dc03820",[4217]="a6826664b45b045991add4756877f304",[4218]="d2601000f00703f56bfc0eb0d6576c5a",[4219]="14029fa9536d06875799225976cefa49",[4220]="68e3459c3ed7af40fbc03c5a98d325ea",[4221]="fbb462d17867f2e9c5d42be507502a2d",[4222]="17b287e0eca51af73ee63d59648b5f2c",[4223]="356f4324639d423aac6b638458c5b115",[4224]="da1f7a35e7a660056aee9161f9855623",[4225]="279ddfa864b91effadd6be0cf9ceaeff",[4226]="e47f0e926018e5f7b45a9cddb4386f18",[4227]="cc1864426c39fe08c6a3e53f2499b25c",[4228]="a5c6ad1ccc9b6b16b94975eea851cf29",[4229]="7feb66ad484a8a92c7750efb893dde7f",[4230]="b68fa6c13809fa76326e538e37cb9de5",[4231]="79f62460fe1fb8dfaee3688a55f70753",[4232]="37d79e29574d7102fef9e67c55c86778",[4233]="5c74c37bbdf480b1b5daa20a346f0652",[4234]="fd85a37d12638a6bdbcdae4c35c002e2",[4235]="33f1171ccdcd8012180a80012b96f22c",[4236]="e0f702cbd6ed819edb1285b7663b789c",[4237]="e0c69880d44b2dc30467065a1b4f2eb3",[4238]="c06eb8475c11a5b292c039b409a811d0",[4239]="4497b4ef86606a15a11809a5ef0ffdbb",[424]="_sea",[4240]="1c432ce6a637b805a14803126ff1bebd",[4241]="c9371a8d5d021aa58e08d39acf9aad5b",[4242]="c096cc9038cb89f66fea858e5594acb3",[4243]="9b7d01edeaaf8c1e80bcfe2818206a3b",[4244]="c14e6007b337bbdb3b372df6f6ebd778",[4245]="f35b143784986718b758e58a712c4420",[4246]="e4ff4adda129db852eae769f3b3faf5b",[4247]="1a025d5bc9bc8719d2dd0aa6c16997c4",[4248]="6938b453d82701c43ff029ecc06ffcb9",[4249]="e3b63a5bd3dd6aac30d1d3e45da12c09",[425]="_sea",[4250]="62a684b635ecc297896fc04d2844d847",[4251]="0c40a07cac098e40c8400d69ce6dc986",[4252]="c9371a8d5d021aa58e08d39acf9aad5b",[4253]="c9371a8d5d021aa58e08d39acf9aad5b",[4254]="c9371a8d5d021aa58e08d39acf9aad5b",[4255]="c9371a8d5d021aa58e08d39acf9aad5b",[426]="_sea",[427]="_sea",[4312]="c9371a8d5d021aa58e08d39acf9aad5b",[4313]="c9371a8d5d021aa58e08d39acf9aad5b",[4314]="c9371a8d5d021aa58e08d39acf9aad5b",[4315]="c9371a8d5d021aa58e08d39acf9aad5b",[4316]="7ae9ecaa7a5b5142f66dc2b88c6901b2",[4317]="91479054c082c42de082952371683c02",[4318]="e0353f3274c9cacb92abaa982a981303",[4319]="25c156c4fa3f0e35643b6fd5a0eb5295",[4320]="4dd98298014cde451fa1768590560817",[4321]="8870830acbd0526f17f2d78b22220017",[4322]="767c6850dfa259fb16e900d3dcb8c53c",[4323]="d7ba3d01a97e79ff39db807834bba466",[4324]="6712cc5e112e2abd2fd77f6a891a5697",[4325]="cd7196572e0a483921396079365588e5",[4326]="24b333c35a7192b5ed2198ae40437955",[4327]="58c81a2657f9d594fbf6188bcda85599",[4328]="d8ed07b3a0c8e924a6af1350e1aac867",[4329]="7d3c1481bcbeee3a82911669a2b9f815",[4330]="Kalimdor\\map43_30",[4331]="Kalimdor\\map43_31",[4332]="Kalimdor\\map43_32",[4333]="Kalimdor\\map43_33",[4334]="61a5d1309118ff51a1de59c1c34def18",[4335]="9c59b617544c4e67a9b852c5e3f54b60",[4336]="d9e9735d65b078d39737e3e426f53176",[4337]="c9371a8d5d021aa58e08d39acf9aad5b",[4338]="c9371a8d5d021aa58e08d39acf9aad5b",[4339]="c9371a8d5d021aa58e08d39acf9aad5b",[434]="_sea",[4340]="e6c593d1d8254927a30a2eb245a11a97",[4344]="8bc7a5a095beecd985434e5b7ea34cbe",[4345]="50fe9d0f0e059f6ce744785e53136ecf",[4346]="7c7a3805ece4ece20fb9af7654798d38",[4347]="d897a8ae514ac640d45b1228d474af56",[4348]="0e40e6f6bcc0ea6e81cbd33cfbb8a835",[4349]="16e14cdc15f4349ee4789f4dff3d1846",[435]="Kalimdor\\map43_5",[4350]="9b9d26803990767e5da3d38da0c272a1",[436]="Kalimdor\\map43_6",[437]="_sea",[4412]="c9371a8d5d021aa58e08d39acf9aad5b",[4413]="c9371a8d5d021aa58e08d39acf9aad5b",[4414]="c9371a8d5d021aa58e08d39acf9aad5b",[4415]="c9371a8d5d021aa58e08d39acf9aad5b",[4416]="c9371a8d5d021aa58e08d39acf9aad5b",[4417]="c9371a8d5d021aa58e08d39acf9aad5b",[4418]="c9371a8d5d021aa58e08d39acf9aad5b",[4419]="a9556293dffb3f81ece7a6e3c044723e",[4420]="00d328436d33ca51350ae5bf57dca86d",[4421]="fc01c6c05e81b0fc3251072a20840eef",[4422]="a5aebc9972f3ce239ae83b5f2b3ac134",[4423]="e1a45cb92a0de7351e312f7272912ef1",[4424]="305322f31187bc7bc15ce2470669a12d",[4425]="bd749761b4b9be17612917393a543052",[4426]="484392bce9171bf299e35148898cebf9",[4427]="99b38dce4a54a753e6147b38702731e1",[4428]="f05adb570d9924b49ba639499c702c70",[4429]="d7e1ed259a786c2d2fcc3beea2ee2120",[4430]="Kalimdor\\map44_30",[4431]="Kalimdor\\map44_31",[4432]="Kalimdor\\map44_32",[4433]="Kalimdor\\map44_33",[4434]="0c53c43f6d79692e9abc92d9cac7a6b5",[4435]="cd17d4a19668bfe6dc753e4de93157c3",[4436]="0dcfae7b681194fd4ec8e2603b303ea4",[4437]="d0b17ea24d7ced3ee8b2a328229d954f",[4438]="c84f528ba6dd02b106d0d6c83f60d041",[4439]="c84f528ba6dd02b106d0d6c83f60d041",[444]="_sea",[4440]="0178e7b0599d56fbf52a063d96b2fec6",[4443]="15fc8067d5f1b251ab0b83f22db95ee3",[4444]="2e0fbbcd4155fc11af24dbde14f72660",[4445]="71c2314d7a22691b020bf2470e10350f",[4446]="65615278053b7ad438dfca2bcd6fdcee",[4447]="3ea654eca4971c254f352b475a82f735",[4448]="93ae24e74f8551972f2acba800133e6f",[4449]="a1c9682eb711b8d2bc6056640be75dd1",[445]="Kalimdor\\map44_5",[4450]="e45a585998e664322f273dc7e1e52e5b",[446]="Kalimdor\\map44_6",[447]="_sea",[4520]="ab4f55935d788ff8307171b374d1a323",[4521]="c06302d284eb8062837a20de57353c44",[4522]="7a678eed364f2f6ca077cd2254fd09ea",[4523]="8d06ecff839cec787d2ebbfea2c6f583",[4524]="41f7a663f62ebad83f83ad603ee66238",[4525]="7c1df4b50c1ccd4b5bb3e80ed7c93c49",[4526]="9f733a57d72053cec57cdcc96fa452ac",[4527]="d0030012dd009252b73c8b8e7f2b84b1",[4528]="81558d1e6f4d1eaa378557e986223ffb",[4529]="2f4b4872d6b1dd5000be49afea48818f",[4530]="Kalimdor\\map45_30",[4531]="Kalimdor\\map45_31",[4532]="Kalimdor\\map45_32",[4533]="Kalimdor\\map45_33",[454]="_sea",[4544]="49a5d33c42f2820ac01be80026d66145",[4545]="cf7620eb7ce25201938522d556d5f093",[4546]="dc838751870ff9eee1f1c1814a0a1404",[4547]="edd3a855eefc082cfba90b8449a96bb3",[4548]="289a8df4e4dfe4fa3773edf238e762e1",[4549]="a0d2e55b96550bb8f16fc4530e8c7ae6",[455]="_sea",[4550]="e45a585998e664322f273dc7e1e52e5b",[456]="_sea",[457]="_sea",[4620]="2395401daa5b076ae7b4c53532dbac86",[4621]="c24b8d0fbabec9102edd29b36d37d378",[4622]="5f7fbdeceb76a1d9f505a36f70670b22",[4623]="dddcf8f34b7d671fe3640afdc8c9de86",[4624]="dc236319b61a12e56aabb503c4d6ca9a",[4625]="0d4c6f53603e011dcf87e109fdbcede7",[4626]="d96ddee51483c6fd74fa11ffa901c356",[4627]="8203076a8bda2700f21df1fd55ea00e9",[4628]="d055a7517e12a1b9791b9ef8b2b90eec",[4629]="9447bdaf502dff775654cc6ef9311423",[4630]="Kalimdor\\map46_30",[4631]="Kalimdor\\map46_31",[4632]="Kalimdor\\map46_32",[4633]="Kalimdor\\map46_33",[4644]="b31c547ef8653a0605ed98f671264ffb",[4645]="e5144f1346f792684b0dfaf63154aee0",[4646]="483871412e8e54f8ec4868d381fe412d",[4647]="7f7babe2dd7f40c84fceddb738943483",[4648]="e64b3917b9240f699012f7b704eacb88",[4649]="2f6d787f69e03967e2ac2114ff81e05a",[4650]="57f5f98d30f7610a2c8c021516ef2a7f",[4720]="c93b05697973305ac7f2481b684ce32d",[4721]="67ba43d493e62a8fad5de319e6d4cb05",[4722]="3f39d6afdd9cdd4c6f33c9ecaf69d5e4",[4723]="7072aaa341355214b814472ed484c280",[4724]="442f84ca3a807d1c7434e3d81c8661eb",[4725]="67ba43d493e62a8fad5de319e6d4cb05",[4726]="67ba43d493e62a8fad5de319e6d4cb05",[4727]="67ba43d493e62a8fad5de319e6d4cb05",[4728]="67ba43d493e62a8fad5de319e6d4cb05",[4729]="67ba43d493e62a8fad5de319e6d4cb05",[4730]="Kalimdor\\map47_30",[4731]="Kalimdor\\map47_31",[4732]="Kalimdor\\map47_32",[4733]="Kalimdor\\map47_33",[4821]="67ba43d493e62a8fad5de319e6d4cb05",[4822]="67ba43d493e62a8fad5de319e6d4cb05",[4823]="67ba43d493e62a8fad5de319e6d4cb05",[4824]="67ba43d493e62a8fad5de319e6d4cb05",[4825]="67ba43d493e62a8fad5de319e6d4cb05"}


Map.MiniMapBlks = {
    [1]={Map.KMB,1908,-1387.5660,-2060.4561},
    [2]={Map.EMB,2420,2930.591080,-1480.211752}
}

--------
-- Get minimap info for map
-- (map id)
-- ret: table, x, y

function Nx.Map:GetMiniInfo (mapId)

	local winfo = self.MapWorldInfo[mapId]
	local id = winfo.MId

	if not id then
		id = floor (mapId / 1000)

		if id == 90 then	-- BGs?
			return
		end

		local info = self.MapInfo[id]
		if not info then
			return
		end
	end

	local t = self.MiniMapBlks[id]

	if not t then		-- "Isle of Quel'Danas"??

--		if NxData.DebugMap then
--			Nx.prt ("GetMiniInfo: missing %s", id)
--		end
		return
	end

	return t,t[3],t[4],t[5] or 1
end

--------
-- Get minimap block file name (256x256 texture)

function Nx.Map:GetMiniBlkName (miniT, x, y)

	local off = x * 100 + y

--	Nx.prtCtrl ("%s, %s, %s = %s", x, y, off, off + miniT[2])

	return miniT[1][off + miniT[2]]
end

-------------------------------------------------------------------------------


--[[ 
-- WorldMapArea.dbc
-- bounds y1, y2, x1, x2
-- 4.0.1 11,1,17,"Barrens",2622.91650391,-7510.41650391,1612.49987793,-5143.75,-1,0,0,0x0,
-- 4.0.3 11,1,17,"Barrens",202.083328247,-5543.75,1810.41662598,-2020.83325195,-1,0,0,0x0,
-- 5.0.4 11,1,17,"Barrens",202.083328247,-5543.75,1810.41662598,-2020.83325195,-1,0,0,0x0,10,20,
-- 20.26656,-524.5772,-322.4962
-- x = (x-maEntry->x1)/((maEntry->x2-maEntry->x1)/100);
-- y = (y-maEntry->y1)/((maEntry->y2-maEntry->y1)/100);    // client y coord from top to down
--
-- MapWorldInfo table calc:
-- Scale = -y2 + y1 / 500
-- X = -y1 / 5
-- Y = -x1 / 5
function Nx.Map:ConvertMapData()

	local data = {}
	NxData.DumpZoneOverlays = data

	local areas = {}
	NxData.DumpMapAreas = areas

	local wma = { Nx.compat:strsplit ("\n", self.WorldMapArea) }
	local wmo = { Nx.compat:strsplit ("\n", self.WorldMapOverlay) }

	for n, s in ipairs (wma) do

		local aid, map, _, aname, ay1, ay2, ax1, ax2 = Nx.compat:strsplit (",", s)
		aid = tonumber (aid)
		map = tonumber (map)

		aname = gsub (aname, '"', "")
		aname = strlower (aname)

		local nxid = Nx.ID2Zone[aid]
		if nxid and nxid > 0 then

			local name, minLvl, maxLvl, faction, cont = Nx.compat:strsplit ("!", Nx.Zones[nxid])

			if faction ,= "3" then	-- Not instance

				ay1 = tonumber (ay1)
				ay2 = tonumber (ay2)
				ax1 = tonumber (ax1)
				ax2 = tonumber (ax2)

				local scale = (-ay2 + ay1) / 500
				if scale > 0 then
					local t = {}
					areas[nxid] = t
					t[1] = scale
					t[2] = -ay1 / 5						-- X
					t[3] = -ax1 / 5						-- Y
					t[4] = aname
				end
			end
		end

		if map == 0 or map == 1 then
--		if map == 648 or map == 646 or map == 730 then			-- Maelstrom
--		if map == 654 then	-- Gilneas
--		if map == 571 or map == 609 then			-- Northrend, DK start

			Nx.prt ("%s %s %s", aid, map, aname)

			local area = {}

			for n, os in ipairs (wmo) do

				-- 84,41,736,0,0,0,"BanethilHollow",175,235,374,221,292,430,375,497,0x0,

				local _, oaid, _, _, _, _, oname, w, h, x, y = Nx.compat:strsplit (",", os)

				oname = gsub (oname, '"', "")

				if tonumber (oaid) == aid and #oname > 0 then
					oname = strlower (oname)
					area[oname] = format ("%s,%s,%s,%s", x, y, w, h)
				end
			end

			if next (area) then	-- Not empty?
				data[aname] = area
			end
		end
	end
end

Nx.Map.WorldMapArea = {906,1000,6500,"DustwallowMarshScenarioAlliance",-3979.16601563,-5037.5,-3468.75,-4175.0,-1,0,0,0x0,0,0,}

Nx.Map.WorldMapOverlay = {3515,806,6517,0,0,0,"",0,0,0,0,526,386,557,423,0x0,}
 ]]
-------------------------------------------------------------------------------
--EOF

















